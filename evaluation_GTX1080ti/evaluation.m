algorithms = {'original', 'v2', 'v2.1'}; % directories
algorithm_names = {'original', 'v2', 'v2.1'};
avg_names = {'avg. orig.', 'avg. v2', 'avg. v2.1'};

bar_colors = [0.9290 0.6940 0.1250; 0.3010 0.7450 0.9330; 0.6350 0.0780 0.1840; 0.4660 0.6740 0.1880];

N = 30;
M = numel(algorithms);
D = 3;

error_R = zeros(N, M);
error_t = zeros(N, M);

times_algo_steps = zeros(M, N, 3);
times_optimization = zeros(M, N, 3);
times_mean_iter = zeros(M, N);

for m = 1:M
    files = dir(['./' algorithms{m}, '/']);
    stride = 0;
    for n = 1:numel(files)
        stride = stride + 1;
        if files(n).isdir == 0 & endsWith(files(n).name, '.mat') == 1 
            stride = stride - 1;
            data = load(['./' algorithms{m}, '/', files(n).name]);
            output = data.output;
            
            R   = output.R;
            R_G = output.R_G;
            t   = output.t;
            t_G = output.t_G;
            
            error_R(n-stride, m) = sqrt(sum(sum((eye(D) - R*pinv(R_G)).^2)));
            error_t(n-stride, m) = sqrt(sum((t - t_G).^2));
            
            times_algo_steps(m, n-stride, :) = [output.times.init, output.times.sigma, output.times.optimization];
            opt_modules_times = sum(output.times.iterations);
            iterations_mean_times = mean(output.times.iterations);
            times_mean_iter(m, n-stride) = iterations_mean_times(2);
            times_optimization(m, n-stride, :) = opt_modules_times(2:end);
        end
    end
end

% Consider in the evaluation only the results that have acceptable accuracy
correct_acc = (error_R < 0.2) & (error_t < 0.1);


% Compute speed-up at different levels of detail of the algorithm (steps/modules)
speed_up_complete = zeros(M-1, N);
speed_up_blocks = zeros(M-1, N, 3);
speed_up_optimization = zeros(M-1, N, 3);
speed_up_coefficients = zeros(M-1, N);
delta_error_R = zeros(M-1, N);
delta_error_t = zeros(M-1, N);
for m = 2:M
    c = m-1;
    speed_up_complete(c, :) = sum(times_algo_steps(1, :, :), 3) ./ sum(times_algo_steps(m, :, :), 3);
    speed_up_coefficients(c, :) = times_mean_iter(1, :) ./ times_mean_iter(m, :);
    speed_up_blocks(c, :, :) = times_algo_steps(1, :, :) ./ times_algo_steps(m, :, :);
    speed_up_optimization(c, :, :) = times_optimization(1, :, :) ./ times_optimization(m, :, :);
    
    delta_error_R(c, :) = (error_R(:, 1) - error_R(:, m))';
    delta_error_t(c, :) = (error_t(:, 1) - error_t(:, m))';
end

A(:, 1) = error_R(:, 1) ./ error_R(:, 2);
A(:, 2) = error_t(:, 1) ./ error_t(:, 2);



for m = 1:M
    avg_time(m) = mean(sum(times_algo_steps(m, correct_acc(:,1), :), 3));
end


%% Accuracy
% Average errors
correct_idx = find(correct_acc(:, 1) == 1);
avg_error_R = [];
avg_error_t = [];
std_error_R = [];
std_error_t = [];
for m = 1:M  % compute errors only in match-success cases
    avg_error_R = [avg_error_R, mean(error_R(correct_acc(:, m), m))];
    avg_error_t = [avg_error_t, mean(error_t(correct_acc(:, m), m))];
    std_error_R = [std_error_R, std(error_R(correct_acc(:, m), m))];
    std_error_t = [std_error_t, std(error_t(correct_acc(:, m), m))];
end

for m = 1:M
    fprintf('%15s: err_t= %f (%f) \t err_r= %f (%f)\n', algorithm_names{m}, ...
        avg_error_t(m), std_error_t(m), avg_error_R(m), std_error_R(m));
end

% Plot errors and deviations from the original errors
figure(1); 
subplot(2,1,1);
bplot = bar(1:numel(correct_idx), error_R(correct_acc(:, 1), :), 'grouped');
for k = 1:size(error_R, 2)
    bplot(k).FaceColor = bar_colors(k, :);
end
xlabel('Point clouds');
ylabel('err_r');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);
legend(algorithm_names);
title('Comparison of rotation errors');

subplot(2,1,2);
bplot = bar(1:numel(correct_idx), delta_error_R(:, correct_acc(:, 1))', 'grouped');
for k = 1:size(delta_error_R, 1)
    bplot(k).FaceColor = bar_colors(k + 1, :);
end
xlabel('Point clouds');
ylabel('delta-err');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);

% Translation errors
figure(2);
subplot(2,1,1);
bplot = bar(1:numel(correct_idx), error_t(correct_acc(:, 1), :), 'grouped');
for k = 1:size(error_t, 2)
    bplot(k).FaceColor = bar_colors(k, :);
end
xlabel('Point cloud ids');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);
ylabel('err_t');
legend(algorithm_names);
title('Comparison of translation errors');

subplot(2,1,2);
bplot = bar(1:numel(correct_idx), delta_error_t(:, correct_acc(:, 1))', 'grouped');
for k = 1:size(delta_error_t, 1)
    bplot(k).FaceColor = bar_colors(k+1, :);
end
xlabel('Point clouds');
ylabel('\Delta err_t');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);

%% Time performance
avg_speedup_complete = mean(speed_up_complete(:, correct_acc(:, 1)), 2);
avg_speedup_coefficients = mean(speed_up_optimization(:, correct_acc(:, 1), 1), 2);
avg_speedup_optim = mean(speed_up_optimization(:, correct_acc(:, 1), 2), 2);
std_speedup_complete = std(speed_up_complete(:, correct_acc(:, 1)), [], 2);
std_speedup_coefficients = std(speed_up_optimization(:, correct_acc(:, 1), 1), [], 2);
std_speedup_optim = std(speed_up_optimization(:, correct_acc(:, 1), 2), [], 2);

fprintf('\n- Speed-up results\n');
for m = 2:M
    fprintf('%15s: total= %1.2fx (%1.2f) \t coeff = %1.2fx (%1.2f) \t optim = %1.2fx (%1.2f) \n', algorithm_names{m}, ...
        avg_speedup_complete(m - 1), std_speedup_complete(m - 1), ...
        avg_speedup_coefficients(m - 1), std_speedup_coefficients(m - 1), ...
        avg_speedup_optim(m - 1), std_speedup_optim(m - 1));
end


figure(3); 
% ------------------
subplot(3,1,1); hold on;
bplot = bar(1:numel(correct_idx), speed_up_complete(:, correct_acc(:, 1))', 'grouped');
plot(ones(1, numel(correct_idx)), 'linewidth', 2, 'color', bar_colors(1, :));
for k = 1:size(speed_up_complete, 1)
    bplot(k).FaceColor = bar_colors(k+1, :);
    plot(1:numel(correct_idx), repmat(avg_speedup_complete(k, :), 1, numel(correct_idx)), '--', 'linewidth', 2, 'color', bar_colors(k+1, :));
end

xlabel('Point clouds');
ylabel('speedup (x)');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);
lgd = legend([algorithm_names(2:end), algorithm_names(1), avg_names(2:end)], 'Orientation', 'horizontal');
legend('boxoff');
title('Overall speedup');

% ------------------
subplot(3,1,2); hold on;
bplot = bar(1:numel(correct_idx), speed_up_optimization(:, correct_acc(:, 1), 1)', 'grouped');
plot(ones(1, numel(correct_idx)), 'linewidth', 2, 'color', bar_colors(1, :));
for k = 1:size(speed_up_complete, 1)
    bplot(k).FaceColor = bar_colors(k+1, :);
    plot(1:numel(correct_idx), repmat(avg_speedup_coefficients(k, :), 1, numel(correct_idx)), '--', 'linewidth', 2, 'color', bar_colors(k+1, :));
end
xlabel('Point clouds');
ylabel('speedup (x)');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);
%lgd = legend([algorithm_names(2:end), algorithm_names(1), avg_names(2:end)], 'Orientation', 'horizontal'); legend('boxoff');
title('Speedup coefficient calculation');

% ------------------
subplot(3,1,3); hold on;
bplot = bar(1:numel(correct_idx), speed_up_optimization(:, correct_acc(:, 2), 2)', 'grouped');
plot(ones(1, numel(correct_idx)), 'linewidth', 2, 'color', bar_colors(1, :));
for k = 1:size(speed_up_complete, 1)
    bplot(k).FaceColor = bar_colors(k+1, :);
    plot(1:numel(correct_idx), repmat(avg_speedup_optim(k, :), 1, numel(correct_idx)), '--', 'linewidth', 2, 'color', bar_colors(k+1, :));
end
xlabel('Point clouds');
ylabel('speedup (x)');
xticks(1:numel(correct_idx));
xticklabels(correct_idx);
%lgd = legend([algorithm_names(2:end), algorithm_names(1), avg_names(2:end)], 'Orientation', 'horizontal'); legend('boxoff');
title('Speedup error-minimization');


