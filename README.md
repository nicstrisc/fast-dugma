# Optimized implementation of DUGMA for 3d point cloud alignment

## Introduction
This repository contains an optimized implementation of the DUGMA algorithm by Pu et al. [1]. Two versions are distributed:

* v2.1: optimization at thread level and w.r.t. memory access
* v3: mixed precision implementation

For more information about the original implementation, go to the [repository of Can Pu](https://github.com/Canpu999/DUGMA)

## Directories
This repository containes the following folders:

* __v2.1__: removed redundant operations in threads, reduced memory access, avoid computations for close-to-0 probability density functions
* __v3__: v2.1 implemented with mixed precision (half floats - 16bits) calculations
* __evaluation_GTX1080ti__: results of the evaluation of the optimized algorithm run on an nVidia GTX 1080ti gpu
* __evaluation_V100__: results of the evaluation of the optimized algorithm run on an nVidia v100 gpu

## How to use
In order to run a simulation, move to one of the folders _v2.1_ or _v3_ and:

1) run compile.m
2) run main_kinect_real_application.m

_Note_: to run version *v3* you need adequate hardware, i.e. a gpu with CC > 7.x 

### References
[1] _Can Pu, Nanbo Li, Radim Tylecek, Robert B Fisher_, "DUGMA: Dynamic Uncertainty-Based Gaussian Mixture Alignment", 3DV 2018

	@inproceedings{pu2018dugma,
	  title={DUGMA: Dynamic Uncertainty-Based Gaussian Mixture 	Alignment},
	  author={Pu, Can and Li, Nanbo and Tylecek, Radim and Fisher, Bob},
	  booktitle={2018 International Conference on 3D Vision (3DV)},
	  pages={766--774},
	  year={2018},
	  organization={IEEE}
	}
	
### Contacts
For questions on the original implementation, contact: can.pu@ed.ac.uk
For questions on this implementation, contact: n.strisciuglio@rug.nl

### Acknowledgements
This code was developed for the [TrimBot2020](http://www.trimbot2020.org) project, funded by the EU Commission in the program Horizon 2020, grant no. 688007