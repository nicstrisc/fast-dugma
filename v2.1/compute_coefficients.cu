#include <cuda_runtime.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>


#define THREAD_POOL_COEFFICIENTS 8

#define MAX_POINT_NUMBER 1000000

#define MAX_SIZE_DATA 3
#define MAX_SIZE_TRAN 12
#define BLOCK_X_SIZE 32
#define BLOCK_Y_SIZE 32

#define times2D 1      // the coefficient matrix in shared memory should be 46*32*times2D
#define times3D 1      // the coefficient matrix in shared memory should be 4*637*times3D


__constant__ int d_Data_Coef[MAX_SIZE_DATA]; //include N,M,D
__constant__ double d_Tran_Coef[MAX_SIZE_TRAN];
__constant__ double d_Sig_Coef[1];

/**
 * @brief Compute the coefficients of the optimization function
 * @param data      MEX parameter matrix
 * @param R         Rotation matrix
 * @param t         Translation vector
 * @param N1        Size of the 1st pointcloud
 * @param M1        Size of the 2nd pointcloud
 * @param D1        Number of dimensions (2 or 3) of the point space
 * @param sig1      Sigma
 * @param cMatrix   Output coefficient matrix
 */
void compute_coefficients(float *data[6], double *R, double *t, double * N, double * M, double * D, double * sig1, double cMatrix[640]);

/**
 * @brief Compute the coefficients of the optimization for 2D point clouds (CUDA kernel)
 * @param d_X       First point cloud
 * @param d_X_C     Uncertainty first point cloud
 * @param det_X     Determinants first point cloud
 * @param d_Y       Second point cloud
 * @param d_Y_C     Uncertainty second point cloud
 * @param det_Y     Determinants second point cloud
 * @param B_C       Result matrix
 */
__global__ void coef2D(float *d_X, float *d_X_C, float *det_X, float *d_Y, float *d_Y_C, float *det_Y, double *B_C);

/**
 * @brief Compute the coefficients of the optimization for 3D point clouds (CUDA kernel)
 * @param d_X       First point cloud
 * @param d_X_C     Uncertainty first point cloud
 * @param det_X     Determinants first point cloud
 * @param d_Y       Second point cloud
 * @param d_Y_C     Uncertainty second point cloud
 * @param det_Y     Determinants second point cloud
 * @param B_C       Result matrix
 */
__global__ void coef3D(float *d_X, float *d_X_C, float *det_X, float *d_Y, float *d_Y_C, float *det_Y, double *B_C);

/**
 * @brief Reduce the partial coefficient results to a single matrix
 * @param d_idata
 * @param coefficient
 * @param size
 * @param n_c
 */
void coeff_reduction(double *d_idata, double *coefficient, int size, int n_c, float norm_factor);

__global__ void coeff_reduction_kernel(double * B_C,int n_c, int L,int stride);

/**
 * @brief set_constants Set constant parameter values into GPU global memory
 * @param N
 * @param M
 * @param D
 * @param sig
 * @param R
 * @param t
 */
void set_constants(int N, int M, int D, double sig, double *R, double *t);

void check(cudaError_t error_id) {
    if (error_id != cudaSuccess) {
        printf("cudaGetDeviceCount returned %d\n-> %s\n",(int)error_id,cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }
}

__forceinline__ __device__ unsigned dynamic_smem_size() {
    unsigned ret;
    asm volatile ("mov.u32 %0, %dynamic_smem_size;" : "=r"(ret));
    return ret;
}



void compute_coefficients(float *data[6], double *R, double *t, double * N1, double * M1, double * D1, double * sig1, double cMatrix[640]) {
    float *d_X, *d_X_C, *d_Y, *d_Y_C, *det_X, *det_Y;
    d_X     = data[0];
    d_X_C   = data[1];
    d_Y     = data[2];
    d_Y_C   = data[3];
    det_X   = data[4];
    det_Y   = data[5];

    int M, N, D;
    N = static_cast<int>(*N1);
    M = static_cast<int>(*M1);
    D = static_cast<int>(*D1);

    set_constants(N, M, D, *sig1, R, t);

    if (M > MAX_POINT_NUMBER || N > MAX_POINT_NUMBER) {
        printf("ERROR: number of points in the pointclouds is too high.");
        exit(EXIT_FAILURE);
    }

    dim3 threadsPerBlock(BLOCK_X_SIZE, BLOCK_Y_SIZE, 1);
    dim3 blocks((N + threadsPerBlock.x - 1) / threadsPerBlock.x,
                (M + threadsPerBlock.y - 1) / threadsPerBlock.y);

    // malloc device global memory for coefficient
    // for 2D, we also use the block(32,32), coefficient matrix in shared memory will be [32*45]
    // the maximum number of coefficient matrixes(4*637 or 32*45) is 4096, 3D:80MB, 2D:46MB
    unsigned int nBytes;
    int NN = blocks.x;  //blocks in X direction
    int MM = blocks.y;  //blocks in Y direction

    int size, n_c;
    if (D == 2) {
        n_c = 46;
        if (NN*MM <= 4096) {
            nBytes = NN * MM * 32 * n_c * sizeof(double)*times2D;
            //the length of B_C
            size=NN*MM*32*n_c*times2D;
        }
        else
        {
            nBytes=4096*32*n_c*sizeof(double)*times2D;
            size=4096*32*n_c*times2D;
        }
    }
    else {
        n_c = 640;
        if (NN * MM <= 4096) {
            nBytes = NN * MM * THREAD_POOL_COEFFICIENTS * n_c * sizeof(double) * times3D; // might be sizeof(float)
            size = NN * MM * THREAD_POOL_COEFFICIENTS * n_c * times3D;
        }
        else {
            nBytes = 4096 * THREAD_POOL_COEFFICIENTS * n_c * sizeof(double) * times3D;
            size = 4096 * THREAD_POOL_COEFFICIENTS * n_c * times3D;
        }
    }

    double *B_C;  // the coefficient box in device global memory
    check(cudaMalloc((void **) &B_C, nBytes));
    check(cudaMemset(B_C, 0, nBytes));
    //check(cudaDeviceSynchronize());

    // malloc the memory for the coefficient
    double *coefficient = (double *) malloc(n_c * sizeof(double));
    memset(coefficient, 0, n_c * sizeof(double));

    if (D==2)
    {
        //coef2D
        coef2D<<<blocks, threadsPerBlock>>>(d_X,d_X_C,det_X,d_Y,d_Y_C,det_Y,B_C);
        check(cudaDeviceSynchronize());
        //reduction
        coeff_reduction(B_C,coefficient,size, n_c, 1 / (float) (N*M));
    }
    else {
        // compute coefficients
        coef3D<<<blocks, threadsPerBlock>>>(d_X, d_X_C, det_X, d_Y, d_Y_C, det_Y, B_C);
        // check memory
        //check(cudaDeviceSynchronize());
        // Reduction of the coefficient partial matrices into the total coefficient matrix
        coeff_reduction(B_C, coefficient, size, n_c, 1 / (float) (N*M));

	// --------------------------------------------------------------------------------------------------
	// In the computation of coefficients, many were computed more than once with the same formulas
	// In this version we compute unique coefficients once, and here we copy the final results into
	// the appropriate vector cells (where duplicate coefficients were computed)
	coefficient[2] = coefficient[1];
	coefficient[4] = coefficient[1];
	coefficient[5] = coefficient[3];
	coefficient[7] = coefficient[3];
	coefficient[8] = coefficient[6];
	coefficient[9] = coefficient[6];
	coefficient[33] = coefficient[28];
	coefficient[40] = coefficient[28];
	coefficient[34] = coefficient[29];
	coefficient[41] = coefficient[29];
	coefficient[32] = coefficient[30];
	coefficient[36] = coefficient[30];
	coefficient[38] = coefficient[31];
	coefficient[44] = coefficient[31];
	coefficient[37] = coefficient[35];
	coefficient[42] = coefficient[35];
	coefficient[43] = coefficient[39];
	coefficient[45] = coefficient[39];
	coefficient[176] = coefficient[46];
	coefficient[188] = coefficient[46];
	coefficient[177] = coefficient[47];
	coefficient[208] = coefficient[47];
	coefficient[196] = coefficient[48];
	coefficient[213] = coefficient[48];
	coefficient[189] = coefficient[49];
	coefficient[209] = coefficient[49];
	coefficient[199] = coefficient[50];
	coefficient[230] = coefficient[50];
	coefficient[225] = coefficient[51];
	coefficient[237] = coefficient[51];
	coefficient[217] = coefficient[52];
	coefficient[232] = coefficient[52];
	coefficient[227] = coefficient[53];
	coefficient[242] = coefficient[53];
	coefficient[239] = coefficient[54];
	coefficient[243] = coefficient[54];
	coefficient[79] = coefficient[73];
	coefficient[141] = coefficient[73];
	coefficient[132] = coefficient[73];
	coefficient[116] = coefficient[73];
	coefficient[111] = coefficient[73];
	coefficient[85] = coefficient[73];
	coefficient[117] = coefficient[73];
	coefficient[133] = coefficient[73];
	coefficient[80] = coefficient[74];
	coefficient[86] = coefficient[74];
	coefficient[112] = coefficient[74];
	coefficient[118] = coefficient[74];
	coefficient[119] = coefficient[74];
	coefficient[134] = coefficient[74];
	coefficient[135] = coefficient[74];
	coefficient[142] = coefficient[74];
	coefficient[81] = coefficient[75];
	coefficient[87] = coefficient[75];
	coefficient[113] = coefficient[75];
	coefficient[120] = coefficient[75];
	coefficient[121] = coefficient[75];
	coefficient[136] = coefficient[75];
	coefficient[137] = coefficient[75];
	coefficient[143] = coefficient[75];
	coefficient[82] = coefficient[76];
	coefficient[88] = coefficient[76];
	coefficient[115] = coefficient[76];
	coefficient[123] = coefficient[76];
	coefficient[124] = coefficient[76];
	coefficient[139] = coefficient[76];
	coefficient[140] = coefficient[76];
	coefficient[146] = coefficient[76];
	coefficient[83] = coefficient[77];
	coefficient[89] = coefficient[77];
	coefficient[122] = coefficient[77];
	coefficient[128] = coefficient[77];
	coefficient[129] = coefficient[77];
	coefficient[144] = coefficient[77];
	coefficient[145] = coefficient[77];
	coefficient[151] = coefficient[77];
	coefficient[84] = coefficient[78];
	coefficient[90] = coefficient[78];
	coefficient[125] = coefficient[78];
	coefficient[130] = coefficient[78];
	coefficient[131] = coefficient[78];
	coefficient[147] = coefficient[78];
	coefficient[148] = coefficient[78];
	coefficient[152] = coefficient[78];
	coefficient[92] = coefficient[91];
	coefficient[95] = coefficient[91];
	coefficient[96] = coefficient[91];
	coefficient[101] = coefficient[91];
	coefficient[102] = coefficient[91];
	coefficient[94] = coefficient[93];
	coefficient[97] = coefficient[93];
	coefficient[98] = coefficient[93];
	coefficient[103] = coefficient[93];
	coefficient[104] = coefficient[93];
	coefficient[100] = coefficient[99];
	coefficient[105] = coefficient[99];
	coefficient[106] = coefficient[99];
	coefficient[107] = coefficient[99];
	coefficient[108] = coefficient[99];
	coefficient[126] = coefficient[109];
	coefficient[149] = coefficient[109];
	coefficient[127] = coefficient[110];
	coefficient[150] = coefficient[110];
	coefficient[138] = coefficient[114];
	coefficient[153] = coefficient[114];
	coefficient[155] = coefficient[154];
	coefficient[157] = coefficient[156];
	coefficient[159] = coefficient[158];
	coefficient[161] = coefficient[160];
	coefficient[163] = coefficient[162];
	coefficient[165] = coefficient[164];
	coefficient[167] = coefficient[166];
	coefficient[169] = coefficient[168];
	coefficient[171] = coefficient[170];
	coefficient[190] = coefficient[178];
	coefficient[210] = coefficient[179];
	coefficient[192] = coefficient[180];
	coefficient[212] = coefficient[181];
	coefficient[195] = coefficient[182];
	coefficient[215] = coefficient[183];
	coefficient[201] = coefficient[184];
	coefficient[219] = coefficient[186];
	coefficient[211] = coefficient[193];
	coefficient[214] = coefficient[198];
	coefficient[216] = coefficient[200];
	coefficient[218] = coefficient[202];
	coefficient[233] = coefficient[204];
	coefficient[221] = coefficient[205];
	coefficient[222] = coefficient[206];
	coefficient[236] = coefficient[207];
	coefficient[235] = coefficient[223];
	coefficient[238] = coefficient[226];
	coefficient[247] = coefficient[244];
	coefficient[248] = coefficient[244];
	coefficient[253] = coefficient[244];
	coefficient[254] = coefficient[244];
	coefficient[259] = coefficient[244];
	coefficient[249] = coefficient[245];
	coefficient[250] = coefficient[245];
	coefficient[255] = coefficient[245];
	coefficient[256] = coefficient[245];
	coefficient[260] = coefficient[245];
	coefficient[251] = coefficient[246];
	coefficient[252] = coefficient[246];
	coefficient[257] = coefficient[246];
	coefficient[258] = coefficient[246];
	coefficient[261] = coefficient[246];
	coefficient[271] = coefficient[262];
	coefficient[280] = coefficient[262];
	coefficient[272] = coefficient[263];
	coefficient[281] = coefficient[263];
	coefficient[265] = coefficient[264];
	coefficient[273] = coefficient[264];
	coefficient[274] = coefficient[264];
	coefficient[282] = coefficient[264];
	coefficient[283] = coefficient[264];
	coefficient[267] = coefficient[266];
	coefficient[275] = coefficient[266];
	coefficient[276] = coefficient[266];
	coefficient[284] = coefficient[266];
	coefficient[285] = coefficient[266];
	coefficient[269] = coefficient[268];
	coefficient[277] = coefficient[268];
	coefficient[278] = coefficient[268];
	coefficient[286] = coefficient[268];
	coefficient[287] = coefficient[268];
	coefficient[279] = coefficient[270];
	coefficient[288] = coefficient[270];
	coefficient[332] = coefficient[290];
	coefficient[333] = coefficient[291];
	coefficient[334] = coefficient[292];
	coefficient[336] = coefficient[293];
	coefficient[335] = coefficient[294];
	coefficient[337] = coefficient[295];
	coefficient[339] = coefficient[296];
	coefficient[318] = coefficient[297];
	coefficient[319] = coefficient[298];
	coefficient[320] = coefficient[299];
	coefficient[321] = coefficient[300];
	coefficient[322] = coefficient[301];
	coefficient[323] = coefficient[302];
	coefficient[324] = coefficient[303];
	coefficient[325] = coefficient[304];
	coefficient[326] = coefficient[305];
	coefficient[327] = coefficient[306];
	coefficient[328] = coefficient[307];
	coefficient[329] = coefficient[308];
	coefficient[330] = coefficient[309];
	coefficient[344] = coefficient[310];
	coefficient[331] = coefficient[311];
	coefficient[345] = coefficient[312];
	coefficient[338] = coefficient[314];
	coefficient[340] = coefficient[315];
	coefficient[341] = coefficient[316];
	coefficient[342] = coefficient[317];
	coefficient[356] = coefficient[355];
	coefficient[358] = coefficient[355];
	coefficient[359] = coefficient[357];
	coefficient[361] = coefficient[357];
	coefficient[362] = coefficient[360];
	coefficient[363] = coefficient[360];
	coefficient[373] = coefficient[366];
	coefficient[274] = coefficient[368];
	coefficient[376] = coefficient[369];
	coefficient[375] = coefficient[370];
	coefficient[378] = coefficient[371];
	coefficient[381] = coefficient[372];
	coefficient[385] = coefficient[380];
	coefficient[386] = coefficient[383];
	coefficient[387] = coefficient[384];
	coefficient[392] = coefficient[391];
	coefficient[394] = coefficient[391];
	coefficient[512] = coefficient[391];
	coefficient[513] = coefficient[391];
	coefficient[524] = coefficient[391];
	coefficient[525] = coefficient[391];
	coefficient[544] = coefficient[391];
	coefficient[545] = coefficient[391];
	coefficient[395] = coefficient[393];
	coefficient[397] = coefficient[393];
	coefficient[532] = coefficient[393];
	coefficient[535] = coefficient[393];
	coefficient[549] = coefficient[393];
	coefficient[553] = coefficient[393];
	coefficient[566] = coefficient[393];
	coefficient[568] = coefficient[393];
	coefficient[398] = coefficient[396];
	coefficient[399] = coefficient[396];
	coefficient[561] = coefficient[396];
	coefficient[563] = coefficient[396];
	coefficient[573] = coefficient[396];
	coefficient[575] = coefficient[396];
	coefficient[578] = coefficient[396];
	coefficient[579] = coefficient[396];
	coefficient[420] = coefficient[400];
	coefficient[431] = coefficient[400];
	coefficient[421] = coefficient[401];
	coefficient[449] = coefficient[401];
	coefficient[438] = coefficient[402];
	coefficient[455] = coefficient[402];
	coefficient[432] = coefficient[403];
	coefficient[450] = coefficient[403];
	coefficient[439] = coefficient[404];
	coefficient[469] = coefficient[404];
	coefficient[466] = coefficient[405];
	coefficient[476] = coefficient[405];
	coefficient[456] = coefficient[406];
	coefficient[470] = coefficient[406];
	coefficient[467] = coefficient[407];
	coefficient[479] = coefficient[407];
	coefficient[477] = coefficient[408];
	coefficient[480] = coefficient[408];
	coefficient[412] = coefficient[409];
	coefficient[415] = coefficient[409];
	coefficient[482] = coefficient[409];
	coefficient[483] = coefficient[409];
	coefficient[487] = coefficient[409];
	coefficient[488] = coefficient[409];
	coefficient[495] = coefficient[409];
	coefficient[496] = coefficient[409];
	coefficient[413] = coefficient[410];
	coefficient[416] = coefficient[410];
	coefficient[484] = coefficient[410];
	coefficient[485] = coefficient[410];
	coefficient[490] = coefficient[410];
	coefficient[491] = coefficient[410];
	coefficient[498] = coefficient[410];
	coefficient[499] = coefficient[410];
	coefficient[414] = coefficient[411];
	coefficient[417] = coefficient[411];
	coefficient[492] = coefficient[411];
	coefficient[493] = coefficient[411];
	coefficient[500] = coefficient[411];
	coefficient[501] = coefficient[411];
	coefficient[504] = coefficient[411];
	coefficient[505] = coefficient[411];
	coefficient[423] = coefficient[422];
	coefficient[433] = coefficient[422];
	coefficient[434] = coefficient[422];
	coefficient[425] = coefficient[424];
	coefficient[451] = coefficient[424];
	coefficient[452] = coefficient[424];
	coefficient[427] = coefficient[426];
	coefficient[440] = coefficient[426];
	coefficient[441] = coefficient[426];
	coefficient[429] = coefficient[428];
	coefficient[457] = coefficient[428];
	coefficient[458] = coefficient[428];
	coefficient[437] = coefficient[436];
	coefficient[453] = coefficient[436];
	coefficient[454] = coefficient[436];
	coefficient[444] = coefficient[443];
	coefficient[459] = coefficient[443];
	coefficient[460] = coefficient[443];
	coefficient[446] = coefficient[445];
	coefficient[461] = coefficient[445];
	coefficient[462] = coefficient[445];
	coefficient[448] = coefficient[447];
	coefficient[472] = coefficient[447];
	coefficient[473] = coefficient[447];
	coefficient[465] = coefficient[464];
	coefficient[474] = coefficient[464];
	coefficient[475] = coefficient[464];
	coefficient[486] = coefficient[481];
	coefficient[494] = coefficient[481];
	coefficient[497] = coefficient[489];
	coefficient[503] = coefficient[489];
	coefficient[506] = coefficient[502];
	coefficient[507] = coefficient[502];
	coefficient[527] = coefficient[508];
	coefficient[564] = coefficient[508];
	coefficient[530] = coefficient[509];
	coefficient[565] = coefficient[509];
	coefficient[533] = coefficient[510];
	coefficient[567] = coefficient[510];
	coefficient[539] = coefficient[511];
	coefficient[570] = coefficient[511];
	coefficient[515] = coefficient[514];
	coefficient[526] = coefficient[514];
	coefficient[529] = coefficient[514];
	coefficient[546] = coefficient[514];
	coefficient[547] = coefficient[514];
	coefficient[517] = coefficient[516];
	coefficient[528] = coefficient[516];
	coefficient[534] = coefficient[516];
	coefficient[548] = coefficient[516];
	coefficient[550] = coefficient[516];
	coefficient[519] = coefficient[518];
	coefficient[531] = coefficient[518];
	coefficient[536] = coefficient[518];
	coefficient[551] = coefficient[518];
	coefficient[552] = coefficient[518];
	coefficient[522] = coefficient[520];
	coefficient[537] = coefficient[520];
	coefficient[542] = coefficient[520];
	coefficient[555] = coefficient[520];
	coefficient[558] = coefficient[520];
	coefficient[556] = coefficient[521];
	coefficient[576] = coefficient[521];
	coefficient[560] = coefficient[523];
	coefficient[577] = coefficient[523];
	coefficient[540] = coefficient[538];
	coefficient[554] = coefficient[538];
	coefficient[559] = coefficient[538];
	coefficient[569] = coefficient[538];
	coefficient[571] = coefficient[538];
	coefficient[543] = coefficient[541];
	coefficient[557] = coefficient[541];
	coefficient[562] = coefficient[541];
	coefficient[572] = coefficient[541];
	coefficient[574] = coefficient[541];
	coefficient[604] = coefficient[580];
	coefficient[634] = coefficient[580];
	coefficient[588] = coefficient[581];
	coefficient[589] = coefficient[581];
	coefficient[609] = coefficient[581];
	coefficient[610] = coefficient[581];
	coefficient[623] = coefficient[581];
	coefficient[590] = coefficient[582];
	coefficient[592] = coefficient[582];
	coefficient[611] = coefficient[582];
	coefficient[613] = coefficient[582];
	coefficient[624] = coefficient[582];
	coefficient[591] = coefficient[583];
	coefficient[593] = coefficient[583];
	coefficient[612] = coefficient[583];
	coefficient[614] = coefficient[583];
	coefficient[625] = coefficient[583];
	coefficient[595] = coefficient[584];
	coefficient[597] = coefficient[584];
	coefficient[616] = coefficient[584];
	coefficient[618] = coefficient[584];
	coefficient[627] = coefficient[584];
	coefficient[594] = coefficient[585];
	coefficient[598] = coefficient[585];
	coefficient[615] = coefficient[585];
	coefficient[619] = coefficient[585];
	coefficient[626] = coefficient[585];
	coefficient[596] = coefficient[586];
	coefficient[599] = coefficient[586];
	coefficient[617] = coefficient[586];
	coefficient[620] = coefficient[586];
	coefficient[628] = coefficient[586];
	coefficient[600] = coefficient[587];
	coefficient[602] = coefficient[587];
	coefficient[621] = coefficient[587];
	coefficient[622] = coefficient[587];
	coefficient[630] = coefficient[587];
	coefficient[605] = coefficient[601];
	coefficient[606] = coefficient[601];
	coefficient[629] = coefficient[601];
	coefficient[631] = coefficient[601];
	coefficient[635] = coefficient[601];
	coefficient[607] = coefficient[603];
	coefficient[608] = coefficient[603];
	coefficient[632] = coefficient[603];
	coefficient[633] = coefficient[603];
	coefficient[636] = coefficient[603];
	// --------------------------------------------------------------------------------------------------
    }

    //check(cudaDeviceSynchronize());

    for (int i = 0; i < n_c; i++)
        cMatrix[i] = *(coefficient + i);

    //check(cudaDeviceSynchronize());

    free(coefficient);
    check(cudaFree(B_C));
    return;
}



void set_constants(int N, int M, int D, double sig, double *R, double *t) {
    const int h_Data_Coef[MAX_SIZE_DATA] = {N,M,D};
    const double h_Sig_Coef[1] = {sig};

    // copy data on host to the constant memory on device
    check(cudaMemcpyToSymbol(d_Sig_Coef, h_Sig_Coef, sizeof(double)));
    check(cudaMemcpyToSymbol(d_Data_Coef, h_Data_Coef, MAX_SIZE_DATA * sizeof(int)));

    check(cudaDeviceSynchronize());

    double h_Tran_Coef[MAX_SIZE_TRAN];
    if (D==2)
    {
        for (int i=0;i<4;i++)
        {
            h_Tran_Coef[i]=*(R+i);
        }
        for (int i=0;i<2;i++)
        {
            h_Tran_Coef[i+4]=*(t+i);
        }
        for (int i=0;i<6;i++)
        {
            h_Tran_Coef[i+6]=0;
        }
    }
    else {
        for (int i=0; i < 9; i++)
            h_Tran_Coef[i] = *(R + i);

        for (int i=0; i < 3; i++)
            h_Tran_Coef[i + 9] = *(t + i);

    }
    check(cudaMemcpyToSymbol(d_Tran_Coef, h_Tran_Coef, MAX_SIZE_TRAN * sizeof(double)));
    check(cudaDeviceSynchronize());
}


__global__ void coef2D(float *d_X,float *d_X_C,float *det_X,float *d_Y,float *d_Y_C,float *det_Y,double *B_C)
{
    int n_c=46;
    int ix,iy,N,M,D;
    ix=blockIdx.x*blockDim.x+threadIdx.x;
    iy=blockIdx.y*blockDim.y+threadIdx.y;
    N=d_Data_Coef[0];
    M=d_Data_Coef[1];
    D=d_Data_Coef[2];

    double sig;
    sig=d_Sig_Coef[0];
    sig=1/sig;
    float Isig;
    Isig=__double2float_rd(sig);

    __shared__ float X[64],X_C[128],detX[32],Y[64],Y_C[128],detY[32];
    // put the data related to x,y into the shared memory
    if ((threadIdx.y==0) && (ix<N))
    {
        X[threadIdx.x*D]=*(d_X+ix*D);
        //printf("X[%d]= %f \n",ix*D,X[threadIdx.x*D]);
        X[threadIdx.x*D+1]=*(d_X+ix*D+1);
        //printf("X[%d]= %f \n",ix*D+1,X[threadIdx.x*D+1]);
        X_C[threadIdx.x*D*D]=*(d_X_C+ix*D*D)*Isig;
        //printf("X_C[%d]= %f \n",threadIdx.x*D*D,X_C[threadIdx.x*D*D]);
        X_C[threadIdx.x*D*D+1]=*(d_X_C+ix*D*D+1)*Isig;
        //printf("X_C[%d]= %f \n",threadIdx.x*D*D+1,X_C[threadIdx.x*D*D+1]);
        X_C[threadIdx.x*D*D+2]=*(d_X_C+ix*D*D+2)*Isig;
        //printf("X_C[%d]= %f \n",threadIdx.x*D*D+2,X_C[threadIdx.x*D*D+2]);
        X_C[threadIdx.x*D*D+3]=*(d_X_C+ix*D*D+3)*Isig;
        //printf("X_C[%d]= %f \n",threadIdx.x*D*D+3,X_C[threadIdx.x*D*D+3]);
        detX[threadIdx.x]=*(det_X+ix)*Isig;
        //printf("det_X[%d]= %f \n",threadIdx.x,detX[threadIdx.x]);
    }

    if ((threadIdx.y==0)  && (blockIdx.y*blockDim.y+threadIdx.x<M))
    {
        Y[threadIdx.x*D]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D);
        //printf("Y[%d]= %f \n",blockIdx.y*blockDim.y*D+threadIdx.x*D,Y[threadIdx.x*D]);
        Y[threadIdx.x*D+1]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+1);
        //printf("Y[%d]= %f \n",blockIdx.y*blockDim.y*D+threadIdx.x*D+1,Y[threadIdx.x*D+1]);
        Y_C[threadIdx.x*D*D]=*(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D)*Isig;
        //printf("Y_C[%d]= %f \n",threadIdx.x*D*D,Y_C[threadIdx.x*D*D]);
        Y_C[threadIdx.x*D*D+1]=*(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D+1)*Isig;
        //printf("Y_C[%d]= %f \n",threadIdx.x*D*D+1,Y_C[threadIdx.x*D*D+1]);
        Y_C[threadIdx.x*D*D+2]=*(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D+2)*Isig;
        //printf("Y_C[%d]= %f \n",threadIdx.x*D*D+2,Y_C[threadIdx.x*D*D+2]);
        Y_C[threadIdx.x*D*D+3]=*(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D+3)*Isig;
        //printf("Y_C[%d]= %f \n",threadIdx.x*D*D+3,Y_C[threadIdx.x*D*D+3]);
        detY[threadIdx.x]=*(det_Y+blockIdx.y*blockDim.y+threadIdx.x)*Isig;
        //printf("detY[%d]= %f \n",threadIdx.x,detY[threadIdx.x]);
    }
    __syncthreads();

    __shared__ double mat[32*times2D][46];

    // initialize all the elements in mat to zero
    if (threadIdx.x*2<n_c)
    {
        //if time2D==1
        mat[threadIdx.y][threadIdx.x*2]=0;
        //printf("hello %f \n",mat[threadIdx.y][threadIdx.x*2]);
        mat[threadIdx.y][threadIdx.x*2+1]=0;
        // printf("hello %f \n",mat[threadIdx.y][threadIdx.x*2+1]);
    }
    __syncthreads();


    //calculate the coefficient

    if (ix<N && iy<M)
    {
        float n1,n2,m1,m2,a11,a12,a21,a22,b11,b12,b21,b22;
        n1=X[2*threadIdx.x];
        n2=X[2*threadIdx.x+1];
        m1=Y[2*threadIdx.y];
        m2=Y[2*threadIdx.y+1];
        // [a11,a12;a21,a22] is inv(Cy)
        // [b11,b12;b21,b22] is inv(Cx)
        a11=Y_C[4*threadIdx.y];
        a21=Y_C[4*threadIdx.y+1];
        a12=Y_C[4*threadIdx.y+2];
        a22=Y_C[4*threadIdx.y+3];
        b11=X_C[4*threadIdx.x];
        b21=X_C[4*threadIdx.x+1];
        b12=X_C[4*threadIdx.x+2];
        b22=X_C[4*threadIdx.x+3];

        double r11,r21,r12,r22,t1,t2; // the old R,t
        r11=d_Tran_Coef[0];
        r21=d_Tran_Coef[1];
        r12=d_Tran_Coef[2];
        r22=d_Tran_Coef[3];
        t1=d_Tran_Coef[4];
        t2=d_Tran_Coef[5];

        // calculate P
        double P=1;
        double dd1,dd2,c11,c12,c21,c22,mal1,mal2;

        //calculate the P
        dd1=n1 - t1 - m1*r11 - m2*r12;
        dd2=n2 - t2 - m1*r21 - m2*r22;
        // [c11,c12;c21,c22]=R*inv(Cy)*R'
        c11=a11*r11*r11 + a22*r12*r12 + a12*r11*r12 + a21*r11*r12;
        c12=a11*r11*r21 + a12*r11*r22 + a21*r12*r21 + a22*r12*r22;
        c21=a11*r11*r21 + a12*r12*r21 + a21*r11*r22 + a22*r12*r22;
        c22=a11*r21*r21 + a22*r22*r22 + a12*r21*r22 + a21*r21*r22;
        mal1=c11*dd1*dd1 + c22*dd2*dd2 + c12*dd1*dd2 + c21*dd1*dd2;

        // calculate (xn-ym)'*inv(Cx)*(xn-ym)
        mal2=b11*dd1*dd1 + b22*dd2*dd2 + b12*dd1*dd2 + b21*dd1*dd2;

        //
        P=detX[threadIdx.x]*detY[threadIdx.y]*(exp(-0.5*mal1)+exp(-0.5*mal2));
        // sum(Q)=1;
        //P=1;

        // update the ym to calculate the P
        dd1=r11*m1+r12*m2+t1;  // the new ym_x
        dd2=r21*m1+r22*m2+t2;  // the new ym_y
        c11= a11*r11*r11 + a22*r12*r12 + a12*r11*r12 + a21*r11*r12; // the new Cy(1,1)
        c12=a11*r11*r21 + a12*r11*r22 + a21*r12*r21 + a22*r12*r22;  // the new Cy(1,2)
        c21=a11*r11*r21 + a12*r12*r21 + a21*r11*r22 + a22*r12*r22;  // the new Cy(2,1)
        c22=a11*r21*r21 + a22*r22*r22 + a12*r21*r22 + a21*r21*r22;  // the new Cy(2,2)
        m1=dd1;
        m2=dd2;
        a11=c11;
        a12=c12;
        a21=c21;
        a22=c22;


        __syncthreads();

        //calculate the coefficient;
        double n1n1 = n1*n1;
        double n2n2 = n2*n2;
        double m1m1 = m1*m1;
        double m2m2 = m2*m2;
        double m1m2 = m1*m2;
        double m1n1 = m1*n1;
        double m1n2 = m1*n2;
        double m2n1 = m2*n1;
        double m2n2 = m2*n2;

        double temp(0);

        int i=0;
        temp =a11*m1m1+ a22*m2m2+ a12*m1m2 + a21*m1m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=1;
        temp =2*a11*m1 + a12*m2 + a21*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=2;
        temp =a12*m1 + a21*m1 + 2*a22*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=3;
        temp =-1* 2*a11*m1n1- a12*m1n2 - a12*m2n1- a21*m1n2 - a21*m2n1- 2*a22*m2n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=4;
        temp =2*a11*m1m1+ 2*a22*m2m2+ 2*a21*m1m2+ 2*a12*m1m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=5;
        temp =-1* 2*a22*m2 - a21*m1 - a12*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=6;
        temp =a21*m2 + a12*m2 + 2*a11*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=7;
        temp =2*a22*m2n1 - a21*m2n2 + a21*m1n1 - a12*m2n2 + a12*m1n1 - 2*a11*m1n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=8;
        temp = a11;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=9;
        temp = a12 + a21;
        atomicAdd(&mat[threadIdx.x][i], P*temp);


        i=10;
        temp =-1* 2*a11*n1 - a12*n2 - a21*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=11;
        temp =a22;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=12;
        temp =-1* a12*n1 - a21*n1 - 2*a22*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=13;
        temp =a11*n1n1+ b11*m1m1+ a22*n2n2+ b22*m2m2+ b12*m1m2  + b21*m1m2+ a12*n1*n2 + a21*n1*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=14;
        temp = a21*m2 + a12*m2 + 2*a11*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=15;
        temp =2*a22*m2+ a21*m1 + a12*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=16;
        temp =-1* 2*a22*m2n2- a21*m2n1- a21*m1n2- a12*m2n1 - a12*m1n2- 2*a11*m1n1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=17;
        temp =-1*a21- a12;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=18;
        temp =-1* 2*a22 + 2*a11;
        atomicAdd(&mat[threadIdx.x][i], P*temp);


        i=19;
        temp =2*a22*n2 + 2*a21*n1 + 2*a12*n1 - 2*a11*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=20;
        temp = a21+ a12;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=21;
        temp =-1*2*a21*n2 + 2*a22*n1 - 2*a12*n2 - 2*a11*n1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=22;
        temp =-1* a12*n1n1 + b12*m1m1+ a12*n2n2 - b12*m2m2- a21*n1n1 + b21*m1m1+ a21*n2n2 - b21*m2m2 - 2*b11*m1m2 + 2*b22*m1m2 + 2*a11*n1*n2 - 2*a22*n1*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=23;
        temp =2*b11*m1+ b12*m2+ b21*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=24;
        temp =b12*m1+ b21*m1+ 2*b22*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=25;
        temp =-1* 2*b11*m1n1- b12*m1n2- b12*m2n1- b21*m1n2- b21*m2n1- 2*b22*m2n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=26;
        temp =a11*m1m1+ a22*m2m2+ a12*m1m2 + a21*m1m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=27;
        temp =-1* a12*m1 - a21*m1 - 2*a22*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);


        i=28;
        temp =2*a11*m1 + a12*m2 + a21*m2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=29;
        temp =-1* 2*a11*m1n2+ a12*m1n1- a12*m2n2+ a21*m1n1- a21*m2n2+ 2*a22*m2n1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=30;
        temp =a22;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=31;
        temp =-1* a12 - a21;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=32;
        temp =-1* 2*a22*n1 + a21*n2 + a12*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=33;
        temp =a11;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=34;
        temp =a21*n1 + a12*n1 - 2*a11*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=35;
        temp =a11*n2n2+ b11*m2m2+ a22*n1n1+ b22*m1m1- b12*m1m2- b21*m1m2- a12*n1*n2- a21*n1*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=36;
        temp =-1* 2*b11*m2+ b12*m1+ b21*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);


        i=37;
        temp =-1* b12*m2- b21*m2+ 2*b22*m1;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=38;
        temp =2*b11*m2n1- b12*m1n1+ b12*m2n2- b21*m1n1 + b21*m2n2 - 2*b22*m1n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=39;
        temp =b11;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=40;
        temp =b12+ b21;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=41;
        temp =-1* 2*b11*n1- b12*n2- b21*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=42;
        temp =b22;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=43;
        temp =-1* b12*n1- b21*n1- 2*b22*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);

        i=44;
        temp =b11*n1n1 + b22*n2n2+ b12*n1*n2+ b21*n1*n2;
        atomicAdd(&mat[threadIdx.x][i], P*temp);
    }
    __syncthreads();

    //check whether the calculation in the last step is correct or not
    //if (blockIdx.x==0 && blockIdx.y==0 && threadIdx.y==0 && threadIdx.x*2<44)
    //{
    //   printf("mat[%d][%d] %f \n",threadIdx.y,threadIdx.x*2,mat[threadIdx.y][threadIdx.x*2]);
    //   printf("mat[%d][%d] %f \n",threadIdx.y,threadIdx.x*2+1,mat[threadIdx.y][threadIdx.x*2+1]);
    //}
    //__syncthreads();

    // copy the coefficient matrix in each block to the global memory
    if (D*threadIdx.x<n_c)
    {
        double index1;
        index1=blockIdx.y*gridDim.x*(32*n_c)+blockIdx.x*(32*n_c)+threadIdx.y*n_c+threadIdx.x*D;
        index1=fmod(index1,4096.0*32.0*46.0);
        int index;
        index=__double2int_rn(index1);
        atomicAdd((B_C+index),mat[threadIdx.y][D*threadIdx.x]);
        atomicAdd((B_C+index+1),mat[threadIdx.y][D*threadIdx.x+1]);
        //printf("B_C[%d]= %f  \n",index % n_c,*(B_C+index));
        //printf("B_C[%d]= %f  \n",(index+1)%n_c,*(B_C+index+1));
    }
    __syncthreads();

}


__global__ void coef3D(float *d_X, float *d_X_C, float *det_X,
                       float *d_Y, float *d_Y_C, float *det_Y,
                       double *B_C) {
    int n_c = 640;
    int ix, iy, N, M, D;
    ix = blockIdx.x * blockDim.x + threadIdx.x;
    iy = blockIdx.y * blockDim.y + threadIdx.y;

    N = d_Data_Coef[0];
    M = d_Data_Coef[1];
    D = d_Data_Coef[2];

    double sig;
    sig=d_Sig_Coef[0];
    sig=1/sig;
    float Isig;
    Isig=__double2float_rn(sig);

    __shared__ float X[96], X_C[288], detX[32], Y[96], Y_C[288], detY[32];
    // put the data related to x,y into the shared memory
    if ((threadIdx.y == 0) && (ix < N)) {
        X[threadIdx.x*D]=*(d_X+ix*D);
        X[threadIdx.x*D+1]=*(d_X+ix*D+1);
        X[threadIdx.x*D+2]=*(d_X+ix*D+2);
        for (int ii=0; ii < 9; ii++)
            X_C[threadIdx.x*D*D+ii]=*(d_X_C+ix*D*D+ii)*Isig;
        detX[threadIdx.x]=*(det_X+ix)*sqrtf(Isig*Isig*Isig);
    }

    //__syncthreads(); //v4
    if ((threadIdx.y == 0)  && (blockIdx.y * blockDim.y + threadIdx.x < M)) {
        Y[threadIdx.x*D]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D);
        Y[threadIdx.x*D+1]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+1);
        Y[threadIdx.x*D+2]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+2);
        for (int ii = 0; ii < 9; ii++)
            Y_C[threadIdx.x*D*D+ii] = *(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D+ii)*Isig;
        detY[threadIdx.x]=*(det_Y+blockIdx.y*blockDim.y+threadIdx.x)*sqrtf(Isig*Isig*Isig);
    }
    __syncthreads();

    __shared__ float mat[THREAD_POOL_COEFFICIENTS * times3D][640];

    // initilize mat to zero
    if (threadIdx.y < THREAD_POOL_COEFFICIENTS)
        for (int ii = 0; ii < 20; ii++)
            mat[threadIdx.y][threadIdx.x * 20 + ii] = 0;

    __syncthreads();

    //calculate the coefficient
    if (ix < N && iy < M) {
        float n1,n2,n3,m1,m2,m3;
        float a11,a12,a13,a21,a22,a23,a31,a32,a33;
        float b11,b12,b13,b21,b22,b23,b31,b32,b33;
        n1=X[3*threadIdx.x];
        n2=X[3*threadIdx.x+1];
        n3=X[3*threadIdx.x+2];
        m1=Y[3*threadIdx.y];
        m2=Y[3*threadIdx.y+1];
        m3=Y[3*threadIdx.y+2];
        // A is inv(Cy)
        // B is inv(Cx)
        a11=Y_C[9*threadIdx.y];
        a21=Y_C[9*threadIdx.y+1];
        a31=Y_C[9*threadIdx.y+2];
        a12=Y_C[9*threadIdx.y+3];
        a22=Y_C[9*threadIdx.y+4];
        a32=Y_C[9*threadIdx.y+5];
        a13=Y_C[9*threadIdx.y+6];
        a23=Y_C[9*threadIdx.y+7];
        a33=Y_C[9*threadIdx.y+8];

        b11=X_C[9*threadIdx.x];
        b21=X_C[9*threadIdx.x+1];
        b31=X_C[9*threadIdx.x+2];
        b12=X_C[9*threadIdx.x+3];
        b22=X_C[9*threadIdx.x+4];
        b32=X_C[9*threadIdx.x+5];
        b13=X_C[9*threadIdx.x+6];
        b23=X_C[9*threadIdx.x+7];
        b33=X_C[9*threadIdx.x+8];

        float r11,r12,r13,r21,r22,r23,r31,r32,r33,t1,t2,t3; // the old R,t
        r11=d_Tran_Coef[0];
        r21=d_Tran_Coef[1];
        r31=d_Tran_Coef[2];
        r12=d_Tran_Coef[3];
        r22=d_Tran_Coef[4];
        r32=d_Tran_Coef[5];
        r13=d_Tran_Coef[6];
        r23=d_Tran_Coef[7];
        r33=d_Tran_Coef[8];

        t1=d_Tran_Coef[9];
        t2=d_Tran_Coef[10];
        t3=d_Tran_Coef[11];

        // calculate P
        float dd1,dd2,dd3;
        float c11,c12,c13,c21,c22,c23,c31,c32,c33,mal1,mal2;

        //calculate the P
        dd1 = n1 - t1 - m1*r11 - m2*r12 - m3*r13;
        dd2 = n2 - t2 - m1*r21 - m2*r22 - m3*r23;
        dd3 = n3 - t3 - m1*r31 - m2*r32 - m3*r33;

        // C = R*inv(Cy)*R'
        c11 = r11*(a11*r11 + a21*r12 + a31*r13) + r12*(a12*r11 + a22*r12 + a32*r13) + r13*(a13*r11 + a23*r12 + a33*r13);
        c12 = r11*(a11*r21 + a21*r22 + a31*r23) + r12*(a12*r21 + a22*r22 + a32*r23) + r13*(a13*r21 + a23*r22 + a33*r23);
        c13 = r11*(a11*r31 + a21*r32 + a31*r33) + r12*(a12*r31 + a22*r32 + a32*r33) + r13*(a13*r31 + a23*r32 + a33*r33);
        c21 = r21*(a11*r11 + a21*r12 + a31*r13) + r22*(a12*r11 + a22*r12 + a32*r13) + r23*(a13*r11 + a23*r12 + a33*r13);
        c22 = r21*(a11*r21 + a21*r22 + a31*r23) + r22*(a12*r21 + a22*r22 + a32*r23) + r23*(a13*r21 + a23*r22 + a33*r23);
        c23 = r21*(a11*r31 + a21*r32 + a31*r33) + r22*(a12*r31 + a22*r32 + a32*r33) + r23*(a13*r31 + a23*r32 + a33*r33);
        c31 = r31*(a11*r11 + a21*r12 + a31*r13) + r32*(a12*r11 + a22*r12 + a32*r13) + r33*(a13*r11 + a23*r12 + a33*r13);
        c32 = r31*(a11*r21 + a21*r22 + a31*r23) + r32*(a12*r21 + a22*r22 + a32*r23) + r33*(a13*r21 + a23*r22 + a33*r23);
        c33 = r31*(a11*r31 + a21*r32 + a31*r33) + r32*(a12*r31 + a22*r32 + a32*r33) + r33*(a13*r31 + a23*r32 + a33*r33);

        // calculate (xn-ym)'*inv(R*Cy*R')*(xn-ym); (xn-ym)'*inv(Cx)*(xn-ym)
        mal1=dd1*(c11*dd1 + c21*dd2 + c31*dd3) + dd2*(c12*dd1 + c22*dd2 + c32*dd3) + dd3*(c13*dd1 + c23*dd2 + c33*dd3);
        mal2=dd1*(b11*dd1 + b21*dd2 + b31*dd3) + dd2*(b12*dd1 + b22*dd2 + b32*dd3) + dd3*(b13*dd1 + b23*dd2 + b33*dd3);

        //
        float P = detX[threadIdx.x] * detY[threadIdx.y] * (exp(-0.5*mal1) + exp(-0.5*mal2)) / (N*M); // adding a Prob. normalization factor (N*M)
        if (P != 0.0) {
            // update the ym to calculate the P
            dd1=r11*m1+r12*m2+r13*m3+t1;  // the new ym_x
            dd2=r21*m1+r22*m2+r23*m3+t2;  // the new ym_y
            dd3=r31*m1+r32*m2+r33*m3+t3;

            m1 = dd1;
            m2 = dd2;
            m3 = dd3;
            a11 = c11;
            a12 = c12;
            a13 = c13;
            a21 = c21;
            a22 = c22;
            a23 = c23;
            a31 = c31;
            a32 = c32;
            a33 = c33;

            float n1n1 = n1*n1;
            float n2n2 = n2*n2;
            float n3n3 = n3*n3;

            float m1m1 = m1*m1;
            float m1m2 = m1*m2;
            float m1m3 = m1*m3;
            float m2m2 = m2*m2;
            float m2m3 = m2*m3;
            float m3m3 = m3*m3;

            float m1n1 = m1*n1;
            float m1n2 = m1*n2;
            float m1n3 = m1*n3;
            float m2n1 = m2*n1;
            float m2n2 = m2*n2;
            float m2n3 = m2*n3;
            float m3n1 = m3*n1;
            float m3n2 = m3*n2;
            float m3n3 = m3*n3;


            //calculate the coefficient;
            int i;
            int j = threadIdx.x & (THREAD_POOL_COEFFICIENTS - 1);

            float temp;

            i=0;
            temp = b11*n1n1 + b22*n2n2 + b33*n3n3 + b12*n1*n2 + b13*n1*n3 + b21*n1*n2 + b23*n2*n3 + b31*n1*n3 + b32*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=1; //i=2; i=4;
            temp = a11*m1m1;
            atomicAdd(&mat[j][i], P*temp);

            i=3; //i=5; i=7;
            temp = a22*m2m2;
            atomicAdd(&mat[j][i], P*temp);

            i=6; //i=8; i=9;
            temp = a33*m3m3;
            atomicAdd(&mat[j][i], P*temp);

            i=10;
            temp = a11*n1n1 + b11*m1m1;
            atomicAdd(&mat[j][i], P*temp);

            i=11;
            temp = b11*m2m2 + a22*n1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=12;
            temp = b11*m3m3 + a33*n1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=13;
            temp = a11*n2n2 + b22*m1m1;
            atomicAdd(&mat[j][i], P*temp);

            i=14;
            temp = a11*n3n3 + b33*m1m1;
            atomicAdd(&mat[j][i], P*temp);

            i=15;
            temp = a22*n2n2 + b22*m2m2;
            atomicAdd(&mat[j][i], P*temp);

            i=16;
            temp = b22*m3m3 + a33*n2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=17;
            temp = a22*n3n3 + b33*m2m2;
            atomicAdd(&mat[j][i], P*temp);

            i=18;
            temp = a33*n3n3 + b33*m3m3;
            atomicAdd(&mat[j][i], P*temp);

            i=19;
            temp = -2*b11*m1n1 - b12*m1n2 - b13*m1n3 - b21*m1n2 - b31*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=20;
            temp = -2*b11*m2n1 - b12*m2n2 - b13*m2n3 - b21*m2n2 - b31*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=21;
            temp = -2*b11*m3n1 - b12*m3n2 - b13*m3n3 - b21*m3n2 - b31*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=22;
            temp = -b12*m1n1 - b21*m1n1 - 2*b22*m1n2 - b23*m1n3 - b32*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=23;
            temp = -b12*m2n1 - b21*m2n1 - 2*b22*m2n2 - b23*m2n3 - b32*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=24;
            temp = -b12*m3n1 - b21*m3n1 - 2*b22*m3n2 - b23*m3n3 - b32*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=25;
            temp = -b13*m1n1 - b23*m1n2 - b31*m1n1 - b32*m1n2 - 2*b33*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=26;
            temp = -b13*m2n1 - b23*m2n2 - b31*m2n1 - b32*m2n2 - 2*b33*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=27;
            temp = -b13*m3n1 - b23*m3n2 - b31*m3n1 - b32*m3n2 - 2*b33*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=28; //i=33; i=40;
            temp = a11*m2m2 + a22*m1m1 + 2*a12*m1m2 + 2*a21*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=29; //i=34; i=41;
            temp = a11*m3m3 + a33*m1m1 + 2*a13*m1m3 + 2*a31*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=30; //i=32; i=36;
            temp = 2*a11*m1m1;
            atomicAdd(&mat[j][i], P*temp);

            i=31; //i=38; i=44;
            temp = a22*m3m3 + a33*m2m2 + 2*a23*m2m3 + 2*a32*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=35; //i=37; i=42;
            temp = 2*a22*m2m2;
            atomicAdd(&mat[j][i], P*temp);

            i=39; //i=43; i=45;
            temp = 2*a33*m3m3;
            atomicAdd(&mat[j][i], P*temp);

            i=46; //i=176; i=188;
            temp = -2*a11*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=47; //i=177; i=208;
            temp = -2*a11*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=48; //i=196; i=213;
            temp = -2*a22*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=49; //i=189; i=209;
            temp = -2*a11*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=50; //i=199; i=230;
            temp = -2*a22*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=51; //i=225; i=237;
            temp = -2*a33*m3n1;
            atomicAdd(&mat[j][i], P*temp);

            i=52; //i=217; i=232;
            temp = -2*a22*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=53; //i=227; i=242;
            temp = -2*a33*m3n2;
            atomicAdd(&mat[j][i], P*temp);

            i=54; //i=239; i=243;
            temp = -2*a33*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=55;
            temp = a12*n1n1 + a21*n1n1 + 2*b11*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=56;
            temp = a13*n1n1 + a31*n1n1 + 2*b11*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=57;
            temp = b12*m1m1 + b21*m1m1 + 2*a11*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=58;
            temp = b12*m2m2 + b21*m2m2 + 2*a22*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=59;
            temp = a23*n1n1 + a32*n1n1 + 2*b11*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=60;
            temp = b12*m3m3 + b21*m3m3 + 2*a33*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=61;
            temp = b13*m1m1 + b31*m1m1 + 2*a11*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=62;
            temp = a12*n2n2 + a21*n2n2 + 2*b22*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=63;
            temp = a13*n2n2 + a31*n2n2 + 2*b22*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=64;
            temp = b13*m2m2 + b31*m2m2 + 2*a22*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=65;
            temp = b13*m3m3 + b31*m3m3 + 2*a33*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=66;
            temp = a23*n2n2 + a32*n2n2 + 2*b22*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=67;
            temp = b23*m1m1 + b32*m1m1 + 2*a11*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=68;
            temp = a12*n3n3 + a21*n3n3 + 2*b33*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=69;
            temp = b23*m2m2 + b32*m2m2 + 2*a22*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=70;
            temp = a13*n3n3 + a31*n3n3 + 2*b33*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=71;
            temp = b23*m3m3 + b32*m3m3 + 2*a33*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=72;
            temp = a23*n3n3 + a32*n3n3 + 2*b33*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=73; //i=79; i=141; i=132; i=116; i=111; i=85; i=117; i=133;
            temp = a12*m1m1 + a21*m1m1 + 2*a11*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=74; //i=80; i=86; i=112; i=118; i=119; i=134; i=135; i=142;
            temp = a12*m2m2 + a21*m2m2 + 2*a22*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=75; //i=81; i=87; i=113; i=120; i=121; i=136; i=137; i=143;
            temp = a13*m1m1 + a31*m1m1 + 2*a11*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=76; //i=82; i=88; i=115; i=123; i=124; i=139; i=140; i=146;
            temp = a13*m3m3 + a31*m3m3 + 2*a33*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=77; //i=83; i=89; i=122; i=128; i=129; i=144; i=145; i=151;
            temp = a23*m2m2 + a32*m2m2 + 2*a22*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=78; //i=84; i=90; i=125; i=130; i=131; i=147; i=148; i=152;
            temp = a23*m3m3 + a32*m3m3 + 2*a33*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=91; //i=92; i=95; i=96; i=101; i=102;
            temp = a12*m1m2 + a21*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=93; //i=94; i=97; i=98; i=103; i=104;
            temp = a13*m1m3 + a31*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=99; //i=100; i=105; i=106; i=107; i=108;
            temp = a23*m2m3 + a32*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=109; //i=126; i=149;
            temp = a12*m3m3 + a21*m3m3 + 2*a13*m2m3 + 2*a23*m1m3 + 2*a31*m2m3 + 2*a32*m1m3 + 2*a33*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=110; //i=127; i=150;
            temp = a13*m2m2 + a31*m2m2 + 2*a12*m2m3 + 2*a21*m2m3 + 2*a22*m1m3 + 2*a23*m1m2 + 2*a32*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=114; //i=138; i=153;
            temp = a23*m1m1 + a32*m1m1 + 2*a11*m2m3 + 2*a12*m1m3 + 2*a13*m1m2 + 2*a21*m1m3 + 2*a31*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=154; //i=155;
            temp = b12*m1m2 + b21*m1m2 + a12*n1*n2 + a21*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=156; //i=157;
            temp = b12*m1m3 + b21*m1m3 + a13*n1*n2 + a31*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=158; //i=159;
            temp = b12*m2m3 + b21*m2m3 + a23*n1*n2 + a32*n1*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=160; //i=161;
            temp = b13*m1m2 + b31*m1m2 + a12*n1*n3 + a21*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=162; //i=163;
            temp = b13*m1m3 + b31*m1m3 + a13*n1*n3 + a31*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=164; //i=165;
            temp = b13*m2m3 + b31*m2m3 + a23*n1*n3 + a32*n1*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=166; //i=167;
            temp = b23*m1m2 + b32*m1m2 + a12*n2*n3 + a21*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=168; //i=169;
            temp = b23*m1m3 + b32*m1m3 + a13*n2*n3 + a31*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=170; //i=171;
            temp = b23*m2m3 + b32*m2m3 + a23*n2*n3 + a32*n2*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=172;
            temp = -2*a11*m2n1 - 2*a12*m1n1 - 2*a21*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=173;
            temp = -2*a12*m2n1 - 2*a21*m2n1 - 2*a22*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=174;
            temp = -2*a11*m3n1 - 2*a13*m1n1 - 2*a31*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=175;
            temp = -2*a13*m3n1 - 2*a31*m3n1 - 2*a33*m1n1;
            atomicAdd(&mat[j][i], P*temp);


            i=178; //i=190;
            temp = -a12*m1n1 - a21*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=179; //i=210;
            temp = -a12*m1n2 - a21*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=180; //i=192;
            temp = -a12*m2n1 - a21*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=181; //i=212;
            temp = -a12*m2n2 - a21*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=182; //i=195;
            temp = -a13*m1n1 - a31*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=183; //i=215;
            temp = -a13*m1n2 - a31*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=184; //i=201;
            temp = -a13*m3n1 - a31*m3n1;
            atomicAdd(&mat[j][i], P*temp);

            i=185;
            temp = -2*a22*m3n1 - 2*a23*m2n1 - 2*a32*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=186; //i=219;
            temp = -a13*m3n2 - a31*m3n2;
            atomicAdd(&mat[j][i], P*temp);

            i=187;
            temp = -2*a23*m3n1 - 2*a32*m3n1 - 2*a33*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=191;
            temp = -2*a11*m2n2 - 2*a12*m1n2 - 2*a21*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=193; //i=211;
            temp = -a12*m1n3 - a21*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=194;
            temp = -2*a12*m2n2 - 2*a21*m2n2 - 2*a22*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=197;
            temp = -2*a11*m3n2 - 2*a13*m1n2 - 2*a31*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=198; //i=214;
            temp = -a12*m2n3 - a21*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=200; //i=216;
            temp = -a13*m1n3 - a31*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=202; //i=218;
            temp = -a23*m2n1 - a32*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=203;
            temp = -2*a13*m3n2 - 2*a31*m3n2 - 2*a33*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=204; //i=233;
            temp = -a23*m2n2 - a32*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=205; //i=221;
            temp = -a23*m3n1 - a32*m3n1;
            atomicAdd(&mat[j][i], P*temp);

            i=206; //i=222;
            temp = -a13*m3n3 - a31*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=207; //i=236;
            temp = -a23*m3n2 - a32*m3n2;
            atomicAdd(&mat[j][i], P*temp);

            i=220;
            temp = -2*a22*m3n2 - 2*a23*m2n2 - 2*a32*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=223; //i=235;
            temp = -a23*m2n3 - a32*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=224;
            temp = -2*a23*m3n2 - 2*a32*m3n2 - 2*a33*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=226; //i=238;
            temp = -a23*m3n3 - a32*m3n3;
            atomicAdd(&mat[j][i], P*temp);

            i=228;
            temp = -2*a11*m2n3 - 2*a12*m1n3 - 2*a21*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=229;
            temp = -2*a12*m2n3 - 2*a21*m2n3 - 2*a22*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=231;
            temp = -2*a11*m3n3 - 2*a13*m1n3 - 2*a31*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=234;
            temp = -2*a13*m3n3 - 2*a31*m3n3 - 2*a33*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=240;
            temp = -2*a22*m3n3 - 2*a23*m2n3 - 2*a32*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=241;
            temp = -2*a23*m3n3 - 2*a32*m3n3 - 2*a33*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=244; //i=247; i=248; i=253; i=254; i=259;
            temp = a12*m1m3 + a13*m1m2 + a21*m1m3 + a31*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=245; //i=249; i=250; i=255; i=256; i=260;
            temp = a12*m2m3 + a21*m2m3 + a23*m1m2 + a32*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=246; //i=251; i=252; i=257; i=258; i=261;
            temp = a13*m2m3 + a23*m1m3 + a31*m2m3 + a32*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=262; //i=271; i=280;
            temp = 2*a11*m2m2 + 2*a22*m1m1 + 2*a12*m1m2 + 2*a21*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=263; //i=272; i=281;
            temp = 2*a11*m3m3 + 2*a33*m1m1 + 2*a13*m1m3 + 2*a31*m1m3;
            atomicAdd(&mat[j][i], P*temp);

            i=264; //i=265; i=273; i=274; i=282; i=283;
            temp = a13*m2m2 + a31*m2m2 + a12*m2m3 + a21*m2m3 + 2*a22*m1m3 + a23*m1m2 + a32*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=266; //i=267; i=275; i=276; i=284; i=285;
            temp = a12*m3m3 + a21*m3m3 + a13*m2m3 + a23*m1m3 + a31*m2m3 + a32*m1m3 + 2*a33*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=268; //i=269; i=277; i=278; i=286; i=287;
            temp = a23*m1m1 + a32*m1m1 + 2*a11*m2m3 + a12*m1m3 + a13*m1m2 + a21*m1m3 + a31*m1m2;
            atomicAdd(&mat[j][i], P*temp);

            i=270; //i=279; i=288;
            temp = 2*a22*m3m3 + 2*a33*m2m2 + 2*a23*m2m3 + 2*a32*m2m3;
            atomicAdd(&mat[j][i], P*temp);

            i=289;
            temp = -2*a12*m3n1 - 2*a13*m2n1 - 2*a21*m3n1 - 2*a23*m1n1 - 2*a31*m2n1 - 2*a32*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=290; //i=332;
            temp = -2*a11*m2n2 - a12*m1n2 - a21*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=291; //i=333;
            temp = -2*a11*m3n2 - a13*m1n2 - a31*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=292; //i=334;
            temp = -a12*m2n2 - a21*m2n2 - 2*a22*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=293; //i=336;
            temp = -a12*m3n2 - a21*m3n2 - a23*m1n2 - a32*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=294; //i=335;
            temp = -a12*m3n2 - a13*m2n2 - a21*m3n2 - a31*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=295; //i=337;
            temp = -a13*m2n2 - a23*m1n2 - a31*m2n2 - a32*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=296; //i=339;
            temp = -a13*m3n2 - a31*m3n2 - 2*a33*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=297; //i=318;
            temp = -2*a11*m2n1 - a12*m1n1 - a21*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=298; //i=319;
            temp = -2*a11*m2n3 - a12*m1n3 - a21*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=299; //i=320;
            temp = -2*a11*m3n1 - a13*m1n1 - a31*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=300; //i=321;
            temp = -a12*m2n1 - a21*m2n1 - 2*a22*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=301; //i=322;
            temp = -2*a11*m3n3 - a13*m1n3 - a31*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=302; //i=323;
            temp = -a12*m2n3 - a21*m2n3 - 2*a22*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=303; //i=324;
            temp = -a12*m3n1 - a13*m2n1 - a21*m3n1 - a31*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=304; //i=325;
            temp = -a12*m3n1 - a21*m3n1 - a23*m1n1 - a32*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=305; //i=326;
            temp = -a13*m2n1 - a23*m1n1 - a31*m2n1 - a32*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=306; //i=327;
            temp = -a12*m3n3 - a21*m3n3 - a23*m1n3 - a32*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=307; //i=328;
            temp = -a12*m3n3 - a13*m2n3 - a21*m3n3 - a31*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=308; //i=329;
            temp = -a13*m2n3 - a23*m1n3 - a31*m2n3 - a32*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=309; //i=330;
            temp = -a13*m3n1 - a31*m3n1 - 2*a33*m1n1;
            atomicAdd(&mat[j][i], P*temp);

            i=310; //i=344;
            temp = -2*a22*m3n2 - a23*m2n2 - a32*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=311; //i=331;
            temp = -a13*m3n3 - a31*m3n3 - 2*a33*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=312; //i=345;
            temp = -a23*m3n2 - a32*m3n2 - 2*a33*m2n2;
            atomicAdd(&mat[j][i], P*temp);

            i=313;
            temp = -2*a12*m3n2 - 2*a13*m2n2 - 2*a21*m3n2 - 2*a23*m1n2 - 2*a31*m2n2 - 2*a32*m1n2;
            atomicAdd(&mat[j][i], P*temp);

            i=314; //i=338;
            temp = -2*a22*m3n1 - a23*m2n1 - a32*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=315; //i=340;
            temp = -2*a22*m3n3 - a23*m2n3 - a32*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=316; //i=341;
            temp = -a23*m3n1 - a32*m3n1 - 2*a33*m2n1;
            atomicAdd(&mat[j][i], P*temp);

            i=317; //i=342;
            temp = -a23*m3n3 - a32*m3n3 - 2*a33*m2n3;
            atomicAdd(&mat[j][i], P*temp);

            i=343;
            temp = -2*a12*m3n3 - 2*a13*m2n3 - 2*a21*m3n3 - 2*a23*m1n3 - 2*a31*m2n3 - 2*a32*m1n3;
            atomicAdd(&mat[j][i], P*temp);

            i=346;
            temp = b11;
            atomicAdd(&mat[j][i], P*temp);

            i=347;
            temp = b22;
            atomicAdd(&mat[j][i], P*temp);

            i=348;
            temp = b33;
            atomicAdd(&mat[j][i], P*temp);

            i=349;
            temp = -2*b11*n1 - b12*n2 - b13*n3 - b21*n2 - b31*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=350;
            temp = -b12*n1 - b21*n1 - 2*b22*n2 - b23*n3 - b32*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=351;
            temp = -b13*n1 - b23*n2 - b31*n1 - b32*n2 - 2*b33*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=352;
            temp = b12 + b21;
            atomicAdd(&mat[j][i], P*temp);

            i=353;
            temp = b13 + b31;
            atomicAdd(&mat[j][i], P*temp);

            i=354;
            temp = b23 + b32;
            atomicAdd(&mat[j][i], P*temp);

            i=355; //i=356; i=358;
            temp = a11;
            atomicAdd(&mat[j][i], P*temp);

            i=357; //i=359; i=361;
            temp = a22;
            atomicAdd(&mat[j][i], P*temp);

            i=360; //i=362; i=363;
            temp = a33;
            atomicAdd(&mat[j][i], P*temp);

            i=364;
            temp = 2*b11*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=365;
            temp = 2*b11*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=366; //i=373;
            temp = b12*m1 + b21*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=367;
            temp = 2*b11*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=368; //i=374;
            temp = b12*m2 + b21*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=369; //i=376;
            temp = b13*m1 + b31*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=370; //i=375;
            temp = b12*m3 + b21*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=371; //i=378;
            temp = b13*m2 + b31*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=372; //i=381;
            temp = b13*m3 + b31*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=377;
            temp = 2*b22*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=379;
            temp = 2*b22*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=380; //i=385;
            temp = b23*m1 + b32*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=382;
            temp = 2*b22*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=383; //i=386;
            temp = b23*m2 + b32*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=384; //i=387;
            temp = b23*m3 + b32*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=388;
            temp = 2*b33*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=389;
            temp = 2*b33*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=390;
            temp = 2*b33*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=391; //i=392; i=394; i=512; i=513; i=524; i=525; i=544; i=545;
            temp = 2*a11*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=393; //i=395; i=397; i=532; i=535; i=549; i=553; i=566; i=568;
            temp = 2*a22*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=396; //i=398; i=399; i=561; i=563; i=573; i=575; i=578; i=579;
            temp = 2*a33*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=400; //i=420; i=431;
            temp = -2*a11*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=401; //i=421; i=449;
            temp = -2*a11*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=402; //i=438; i=455;
            temp = -2*a22*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=403; //i=432; i=450;
            temp = -2*a11*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=404; //i=439; i=469;
            temp = -2*a22*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=405; //i=466; i=476;
            temp = -2*a33*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=406; //i=456; i=470;
            temp = -2*a22*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=407; //i=467; i=479;
            temp = -2*a33*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=408; //i=477; i=480;
            temp = -2*a33*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=409; //i=412; i=415; i=482; i=483; i=487; i=488; i=495; i=496;
            temp = a12 + a21;
            atomicAdd(&mat[j][i], P*temp);

            i=410; //i=413; i=416; i=484; i=485; i=490; i=491; i=498; i=499;
            temp = a13 + a31;
            atomicAdd(&mat[j][i], P*temp);

            i=411; //i=414; i=417; i=492; i=493; i=500; i=501; i=504; i=505;
            temp = a23 + a32;
            atomicAdd(&mat[j][i], P*temp);

            i=418;
            temp = -2*a12*n1 - 2*a21*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=419;
            temp = -2*a13*n1 - 2*a31*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=422; //i=423; i=433; i=434;
            temp = -a12*n1 - a21*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=424; //i=425; i=451; i=452;
            temp = -a12*n2 - a21*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=426; //i=427; i=440; i=441;
            temp = -a13*n1 - a31*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=428; //i=429; i=457; i=458;
            temp = -a13*n2 - a31*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=430;
            temp = -2*a23*n1 - 2*a32*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=435;
            temp = -2*a12*n2 - 2*a21*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=436; //i=437; i=453; i=454;
            temp = -a12*n3 - a21*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=442;
            temp = -2*a13*n2 - 2*a31*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=443; //i=444; i=459; i=460;
            temp = -a13*n3 - a31*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=445; //i=446; i=461; i=462;
            temp = -a23*n1 - a32*n1;
            atomicAdd(&mat[j][i], P*temp);

            i=447; //i=448; i=472; i=473;
            temp = -a23*n2 - a32*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=463;
            temp = -2*a23*n2 - 2*a32*n2;
            atomicAdd(&mat[j][i], P*temp);

            i=464; //i=465; i=474; i=475;
            temp = -a23*n3 - a32*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=468;
            temp = -2*a12*n3 - 2*a21*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=471;
            temp = -2*a13*n3 - 2*a31*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=478;
            temp = -2*a23*n3 - 2*a32*n3;
            atomicAdd(&mat[j][i], P*temp);

            i=481; //i=486; i=494;
            temp = 2*a11;
            atomicAdd(&mat[j][i], P*temp);

            i=489; //i=497; i=503;
            temp = 2*a22;
            atomicAdd(&mat[j][i], P*temp);

            i=502; //i=506; i=507;
            temp = 2*a33;
            atomicAdd(&mat[j][i], P*temp);

            i=508; //i=527; i=564;
            temp = 2*a11*m2 + 2*a12*m1 + 2*a21*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=509; //i=530; i=565;
            temp = 2*a12*m2 + 2*a21*m2 + 2*a22*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=510; //i=533; i=567;
            temp = 2*a11*m3 + 2*a13*m1 + 2*a31*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=511; //i=539; i=570;
            temp = 2*a13*m3 + 2*a31*m3 + 2*a33*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=514; //i=515; i=526; i=529; i=546; i=547;
            temp = a12*m1 + a21*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=516; //i=517; i=528; i=534; i=548; i=550;
            temp = a12*m2 + a21*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=518; //i=519; i=531; i=536; i=551; i=552;
            temp = a13*m1 + a31*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=520; //i=522; i=537; i=542; i=555; i=558;
            temp = a13*m3 + a31*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=521; //i=556; i=576;
            temp = 2*a22*m3 + 2*a23*m2 + 2*a32*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=523; //i=560; i=577;
            temp = 2*a23*m3 + 2*a32*m3 + 2*a33*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=538; //i=540; i=554; i=559; i=569; i=571;
            temp = a23*m2 + a32*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=541; //i=543; i=557; i=562; i=572; i=574;
            temp = a23*m3 + a32*m3;
            atomicAdd(&mat[j][i], P*temp);

            i=580; //i=604; i=634;
            temp = 2*a12*m3 + 2*a13*m2 + 2*a21*m3 + 2*a23*m1 + 2*a31*m2 + 2*a32*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=581; //i=588; i=589; i=609; i=610; i=623;
            temp = 2*a11*m2 + a12*m1 + a21*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=582; //i=590; i=592; i=611; i=613; i=624;
            temp = 2*a11*m3 + a13*m1 + a31*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=583; //i=591; i=593; i=612; i=614; i=625;
            temp = a12*m2 + a21*m2 + 2*a22*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=584; //i=595; i=597; i=616; i=618; i=627;
            temp = a12*m3 + a21*m3 + a23*m1 + a32*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=585; //i=594; i=598; i=615; i=619; i=626;
            temp = a12*m3 + a13*m2 + a21*m3 + a31*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=586; //i=596; i=599; i=617; i=620; i=628;
            temp = a13*m2 + a23*m1 + a31*m2 + a32*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=587; //i=600; i=602; i=621; i=622; i=630;
            temp = a13*m3 + a31*m3 + 2*a33*m1;
            atomicAdd(&mat[j][i], P*temp);

            i=601; //i=605; i=606; i=629; i=631; i=635;
            temp = 2*a22*m3 + a23*m2 + a32*m2;
            atomicAdd(&mat[j][i], P*temp);

            i=603; //i=607; i=608; i=632; i=633; i=636;
            temp = a23*m3 + a32*m3 + 2*a33*m2;
            atomicAdd(&mat[j][i], P*temp);
        }
    }
    __syncthreads();

    // Copy the coefficient matrix in each block to the global memory
    if (M >= THREAD_POOL_COEFFICIENTS) {
        if (threadIdx.y < THREAD_POOL_COEFFICIENTS) {
            double index1;
            index1 = blockIdx.y * gridDim.x * (THREAD_POOL_COEFFICIENTS * n_c ) +
                    blockIdx.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                    threadIdx.y * n_c + threadIdx.x * 20;

            index1 = fmod(index1, 4096.0 * THREAD_POOL_COEFFICIENTS * n_c);
            int index = __double2int_rn(index1);
            for (int ii = 0; ii < 20; ii++)
                atomicAdd((B_C + index + ii), mat[threadIdx.y][threadIdx.x*20 +ii]);
        }
    } else {
        if (threadIdx.y==0) {
            int index;
            for (int jj=0; jj < THREAD_POOL_COEFFICIENTS; jj++) {
                double index1 = blockIdx.y * gridDim.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                                blockIdx.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                                jj * n_c + threadIdx.x * 20;
                index1 = fmod(index1, 4096.0 * THREAD_POOL_COEFFICIENTS * n_c);
                index = __double2int_rn(index1);

                for (int ii = 0; ii < 20; ii++)
                    atomicAdd((B_C + index + ii), mat[jj][threadIdx.x*20 + ii]);
            }
        }
    }

    __syncthreads();
    return;
}

void coeff_reduction(double *d_idata, double *coefficient, int size, int n_c, float norm_factor) {
    int t = size / n_c;
    int stride = n_c;
    int blocksize = 1024;
    dim3 block(blocksize, 1);
    while (t > 1) {
        // there are t numbers to deal with
        dim3 grid(((t + 1)/2 + block.x - 1) / block.x, 1);
        // kernel
        coeff_reduction_kernel<<<grid, block>>>(d_idata, n_c, t, stride);
        cudaDeviceSynchronize();
        //update the state
        t = (t + 1) / 2;
        stride = stride * 2;
    }
    int bytes = n_c * sizeof(double);

    cudaMemcpy(coefficient, d_idata, bytes, cudaMemcpyDeviceToHost);
    //cudaDeviceSynchronize();

    // normalization of the coefficients (value of coeffs does not depend on the number of points)
    for (int i = 0; i < n_c; i++)
        coefficient[i] = coefficient[i] * norm_factor;
}

__global__ void coeff_reduction_kernel(double * B_C, int n_c, int L, int stride) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < (L + 1)/2) {
        if (((L&1) == 0) || (idx != (L + 1)/2 - 1))
        {
            for (int i=0; i <n_c; i++)
                atomicAdd((B_C + idx*2*stride + i), *(B_C + idx*2*stride + stride + i));
        }
    }
    __syncthreads();
}

