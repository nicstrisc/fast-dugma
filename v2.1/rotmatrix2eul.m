function [eul]= rotmatrix2eul(R)
eul(1) = atan(R(2,1) / R(1,1));
eul(2) = asin(-R(3,1));
eul(3) = asin(R(3,2) / cos(asin(-R(3,1))));

