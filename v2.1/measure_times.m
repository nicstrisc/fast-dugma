clear all;         
Max_Iteration = 300;
accuracy_rotation = 0.0001;
accuracy_translation = 0.00000001;
accuracy_sig = 0.000000001;

nscenes = 30; % from the Kinect data set
nruns = 1;
out = './exp_results/speedup_2.1/';
if ~exist(out)
    mkdir(out)
end

for k = 1:nscenes
    load(['./dataset_all/Kinect_Application/', num2str(k),'.mat']); 
    R0 = eye(3); 
    t0 = zeros(3,1);
    
    for n = 1:nruns
        t1 = tic;
        [R, t, times] = DUGMA(X, Y, X_C, Y_C, R0, t0, Max_Iteration, accuracy_rotation, accuracy_translation, accuracy_sig);
        times.dugma_func = toc(t1);
        
        D = size(X,1);
        
        output.R = R;
        output.R_G = R_G;
        output.t = t;
        output.t_G = t_G;
        output.times = times;
        
        save([out, num2str(k), '_' num2str(n) '.mat'], 'output');
        
        fprintf('PC: %d \t r: %d \t - time: %f \t eR: %f \t eT: %f \n', k, n, ...
                times.dugma_func, ...
                sqrt(sum(sum((eye(D)-R*pinv(R_G)).^2))), ...
                sqrt(sum((t-t_G).^2)));
    end
    
    
    
end
            
            % figure 1 in our paper
            %load([pwd,'/dataset_all/Kinect_Application/',num2str(20),'.mat']);  R0=eye(3); t0=zeros(3,1);        
     
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Register two point cloud
    % Error
    
    %error_R=sqrt(sum(sum((eye(D)-R*pinv(R_G)).^2)))
    %error_t=sqrt(sum((t-t_G).^2))









