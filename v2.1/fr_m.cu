#include <cuda_runtime.h>
#include <stdio.h>

void check(cudaError_t error_id) {
    if (error_id != cudaSuccess) {
        printf("cudaGetDeviceCount returned %d\n-> %s\n",(int)error_id,cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }
}

void fr_m(float *data[6]) {
    // free device memory
    // free temporal device global memory
    check(cudaFree(data[0]));
    check(cudaFree(data[1]));
    check(cudaFree(data[2]));
    check(cudaFree(data[3]));
    check(cudaFree(data[4]));
    check(cudaFree(data[5]));
    check(cudaDeviceReset());
    return;
}
