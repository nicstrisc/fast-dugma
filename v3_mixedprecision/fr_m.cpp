#include "mex.h"
#include "class_handle.hpp"


#define output        plhs[0]

void fr_m(float *data[6]);
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    float *data[6];
    data[0] = convertMat2Ptr<float>(prhs[0]); // d_X
    data[1] = convertMat2Ptr<float>(prhs[1]); // d_X_C;
    data[2] = convertMat2Ptr<float>(prhs[2]); // d_Y;
    data[3] = convertMat2Ptr<float>(prhs[3]); // d_Y_C;
    data[4] = convertMat2Ptr<float>(prhs[4]); // det_X;
    data[5] = convertMat2Ptr<float>(prhs[5]); // det_Y;

    // free device memory
    fr_m(data);

    // Got here, so command not recognized
    output     = mxCreateDoubleMatrix(1, 1, mxREAL);
    double *out = mxGetPr(output);
    *out = 1;

    return;
}
