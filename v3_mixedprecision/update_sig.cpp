#include "mex.h"
#include "class_handle.hpp"


#define OUT_Coef        plhs[0]

void compute_updated_sigma(float *data[6], double *R, double *t, double *N, double *M, double *D, float sig[1]);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    float *data[6];
    data[0] = convertMat2Ptr<float>(prhs[0]); // d_X
    data[1] = convertMat2Ptr<float>(prhs[1]); // d_X_C;
    data[2] = convertMat2Ptr<float>(prhs[2]); // d_Y;
    data[3] = convertMat2Ptr<float>(prhs[3]); // d_Y_C;
    data[4] = convertMat2Ptr<float>(prhs[4]); // det_X;
    data[5] = convertMat2Ptr<float>(prhs[5]); // det_Y;

    double *R, *t, *M, *N, *D;
    R     = mxGetPr(prhs[6]);
    t     = mxGetPr(prhs[7]);
    N     = mxGetPr(prhs[8]);
    M     = mxGetPr(prhs[9]);    
    D     = mxGetPr(prhs[10]);

    float sig[1];

    compute_updated_sigma(data, R, t, N, M, D, sig);

    // output
    OUT_Coef     = mxCreateDoubleMatrix(1, 1, mxREAL);   

    double *sig_output;
    sig_output  = mxGetPr(OUT_Coef);
    *sig_output = sig[0];

    // prepare function return values to send back to MATLAB
    plhs[1] = convertPtr2Mat<float >(*data);
    plhs[2] = convertPtr2Mat<float >(*(data + 1));
    plhs[3] = convertPtr2Mat<float >(*(data + 2));
    plhs[4] = convertPtr2Mat<float >(*(data + 3));
    plhs[5] = convertPtr2Mat<float >(*(data + 4));
    plhs[6] = convertPtr2Mat<float >(*(data + 5));
    return;
}
