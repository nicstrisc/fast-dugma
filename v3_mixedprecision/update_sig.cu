
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>

#define MAX_POINT_NUMBER 1000000

#define BLOCK_X_SIZE 32
#define BLOCK_Y_SIZE 32

#define MAX_SIZE_DATA 3 
#define MAX_SIZE_TRAN 12

#define times2D 1      // the coefficient matrix in shared memory should be 44*32*times2D
#define times3D 2      // the coefficient matrix in shared memory should be 4*637*times3D   
#define SCALE_FACTOR 1000000;

__constant__ int d_Data_Coef[MAX_SIZE_DATA]; //include N,M,D
__constant__ double d_Tran_Coef[MAX_SIZE_TRAN];


void check(cudaError_t error_id) {
    if (error_id != cudaSuccess) {
        printf("cudaGetDeviceCount returned %d\n-> %s\n",(int)error_id, cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief compute_dist3
 * @param d_X
 * @param d_Y
 * @param distance_X
 * @param distance_Y
 */
__global__ void compute_dist3(float *d_X,float *d_Y,  uint *distance_X,uint *distance_Y);

/**
 * @brief compute_dist2
 * @param d_X
 * @param d_Y
 * @param distance_X
 * @param distance_Y
 */
__global__ void compute_dist2(float *d_X,float *d_Y,  uint *distance_X,uint *distance_Y);


/**
 * @brief set_constants Set constant parameter values into GPU global memory
 * @param N
 * @param M
 * @param D
 * @param sig
 * @param R
 * @param t
 */
void set_constants(int N, int M, int D, double *R, double *t);


void compute_distance_reduction(float *d_idata,float *coefficient,int size, int n_c);


__global__ void init_distX(uint *distance_X);

__global__ void init_distY(uint *distance_Y);

__global__ void convert_back(uint *distance_X,float * distance_XX);


void compute_updated_sigma(float *data[6], double *R, double *t, double *N1, double *M1, double *D1, float sig[1]) {
    float *d_X, *d_Y;
    d_X     = data[0];
    d_Y     = data[2];

    int M, N, D;
    N = static_cast<int>(*N1);
    M = static_cast<int>(*M1);
    D = static_cast<int>(*D1);

    set_constants(N, M, D, R, t);

    if (M > MAX_POINT_NUMBER || N > MAX_POINT_NUMBER) {
        printf("ERROR: number of points in the pointclouds is too high.");
        exit(EXIT_FAILURE);
    }

    dim3 threadsPerBlock(BLOCK_X_SIZE, BLOCK_Y_SIZE, 1);
    dim3 blocks((N + threadsPerBlock.x - 1) / threadsPerBlock.x,
                (M + threadsPerBlock.y - 1) / threadsPerBlock.y);


    uint *distance_X, *distance_Y;
    check(cudaMalloc((void **) &distance_X, N * sizeof(uint)));
    check(cudaMalloc((void **) &distance_Y, M * sizeof(uint)));

    // initialization of distance matrices
    init_distX<<<blocks.x, BLOCK_X_SIZE>>>(distance_X);
    init_distY<<<blocks.y, BLOCK_X_SIZE>>>(distance_Y);
    check(cudaDeviceSynchronize());

    // caculate the minimum distance
    if (D == 2)
        compute_dist2<<<blocks, threadsPerBlock>>>(d_X, d_Y, distance_X, distance_Y);
    else if (D == 3)
        compute_dist3<<<blocks, threadsPerBlock>>>(d_X, d_Y, distance_X, distance_Y);

    check(cudaDeviceSynchronize());

    //
    float *distance_XX;
    check(cudaMalloc((void **)&distance_XX, N * sizeof(float)));
    convert_back<<<blocks.x, BLOCK_X_SIZE>>>(distance_X, distance_XX);
    check(cudaDeviceSynchronize());

    // calculate the sum
    float *sig1 = (float*) malloc(sizeof(float));
    compute_distance_reduction(distance_XX, sig1, N,1);
    sig[0]=(*sig1)/N;

    check(cudaDeviceSynchronize());

    free(sig1);
    check(cudaFree(distance_XX));
    check(cudaFree(distance_X));
    check(cudaFree(distance_Y));
    return;
}



void set_constants(int N, int M, int D, double *R, double *t) {
    const int h_Data_Coef[MAX_SIZE_DATA] = {N,M,D};
    // copy data on host to the constant memory on device
    check(cudaMemcpyToSymbol(d_Data_Coef, h_Data_Coef, MAX_SIZE_DATA * sizeof(int)));
    check(cudaDeviceSynchronize());
    
    double h_Tran_Coef[MAX_SIZE_TRAN];
    int i;
    if (D==2) {
        for (i = 0; i < 4; i++)
            h_Tran_Coef[i] = *(R + i);
        for (i = 0; i < 2; i++)
            h_Tran_Coef[i + 4] = *(t + i);
        for (i = 0; i < 6; i++)
            h_Tran_Coef[i + 6] = 0;
    } else {
        for (i = 0; i < 9; i++)
            h_Tran_Coef[i] = *(R + i);
        for (i = 0; i < 3; i++)
            h_Tran_Coef[i + 9] = *(t + i);
    }
    check(cudaMemcpyToSymbol(d_Tran_Coef, h_Tran_Coef, MAX_SIZE_TRAN * sizeof(double)));
    check(cudaDeviceSynchronize());
}


__global__ void init_distX(uint *distance_X) {
    int ix = blockIdx.x*blockDim.x + threadIdx.x;
    int N = d_Data_Coef[0];
    if (ix < N)
        *(distance_X + ix) = UINT_MAX;
}

__global__ void init_distY(uint *distance_Y) {
    int ix = blockIdx.x*blockDim.x + threadIdx.x;
    int M = d_Data_Coef[1];
    if (ix < M)
        *(distance_Y + ix) = UINT_MAX;
}

__global__ void compute_dist2(float *d_X,float *d_Y,  uint *distance_X,uint *distance_Y){
    int ix,iy,N,M,D;
    ix=blockIdx.x*blockDim.x+threadIdx.x;
    iy=blockIdx.y*blockDim.y+threadIdx.y;
    N=d_Data_Coef[0];
    M=d_Data_Coef[1];
    D=d_Data_Coef[2];
    double r11,r21,r12,r22,t1,t2; // the old R,t
    r11=d_Tran_Coef[0];
    r21=d_Tran_Coef[1];
    r12=d_Tran_Coef[2];
    r22=d_Tran_Coef[3];
    t1=d_Tran_Coef[4];
    t2=d_Tran_Coef[5]; 
    
    __shared__ float X[64],Y[64];
    
    // put the data related to x,y into the shared memory
    if ((threadIdx.y==0) && (ix<N))
    {
        X[threadIdx.x*D]=*(d_X+ix*D);
        
        X[threadIdx.x*D+1]=*(d_X+ix*D+1);
        
    }
    
    if ((threadIdx.y==0)  && (blockIdx.y*blockDim.y+threadIdx.x<M))
    {
        Y[threadIdx.x*D]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D);
        //printf("Y[%d]= %f \n",blockIdx.y*blockDim.y*D+threadIdx.x*D,Y[threadIdx.x*D]);         
        Y[threadIdx.x*D+1]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+1);
        //printf("Y[%d]= %f \n",blockIdx.y*blockDim.y*D+threadIdx.x*D+1,Y[threadIdx.x*D+1]); 
        
    }             
    __syncthreads();
    
    __shared__ uint dist_X[32]; //, dist_Y[32];

    
    // initialize all the elements in mat to max (minumin computation)
    dist_X[threadIdx.x] = UINT_MAX;
    //dist_Y[threadIdx.x] = UINT_MAX;
    
    __syncthreads();
    
    //calculate the minimum distance: x to every point in Y000
    if (ix < N && iy < M) {
        float n1,n2,m1,m2,dd1,dd2;
        n1=X[2*threadIdx.x];
        n2=X[2*threadIdx.x+1];
        m1=Y[2*threadIdx.y];
        m2=Y[2*threadIdx.y+1];
        dd1=r11*m1+r12*m2+t1;       
        dd2=r21*m1+r22*m2+t2;
        m1=dd1;
        m2=dd2;       
        float s;
        float DD=D;
        unsigned int ss;
        s=((n1-m1)*(n1-m1)+(n2-m2)*(n2-m2)) / DD * SCALE_FACTOR;
        // avoid the race condition
        ss=__float2uint_rn(s);
        atomicMin(&dist_X[threadIdx.x],ss);
    }
    
    __syncthreads();  
    if ((ix<N) && (iy<M) && (threadIdx.y==0))
        atomicMin(distance_X + ix, dist_X[threadIdx.x]);

    __syncthreads(); 
    
}


__global__ void compute_dist3(float *d_X,float *d_Y,  uint *distance_X, uint *distance_Y) {
    int ix, iy, N, M, D;
    float r11, r12, r13, r21, r22, r23, r31, r32, r33, t1, t2, t3; // the old R,t
    r11 = d_Tran_Coef[0];
    r21 = d_Tran_Coef[1];
    r31 = d_Tran_Coef[2];
    r12 = d_Tran_Coef[3];
    r22 = d_Tran_Coef[4];
    r32 = d_Tran_Coef[5];
    r13 = d_Tran_Coef[6];
    r23 = d_Tran_Coef[7];
    r33 = d_Tran_Coef[8];
    t1  = d_Tran_Coef[9];
    t2  = d_Tran_Coef[10];
    t3  = d_Tran_Coef[11];
    
    __shared__ float X[96], Y[96];
    // thread coordinates
    ix = blockIdx.x*blockDim.x + threadIdx.x;
    iy = blockIdx.y*blockDim.y + threadIdx.y;

    N = d_Data_Coef[0];
    M = d_Data_Coef[1];
    D = d_Data_Coef[2];

    // put the data related to x,y into the shared memory
    if ((threadIdx.y == 0) && (ix < N)) {
        X[threadIdx.x*D] = *(d_X + ix*D);
        X[threadIdx.x*D+1] = *(d_X + ix*D + 1);
        X[threadIdx.x*D+2] = *(d_X + ix*D + 2);
    }
    
    if ((threadIdx.y == 0)  && (blockIdx.y*blockDim.y + threadIdx.x < M)) {
        Y[threadIdx.x*D]    = *(d_Y + blockIdx.y*blockDim.y*D + threadIdx.x*D);
        Y[threadIdx.x*D+1]  = *(d_Y + blockIdx.y*blockDim.y*D + threadIdx.x*D + 1);
        Y[threadIdx.x*D+2]  = *(d_Y + blockIdx.y*blockDim.y*D + threadIdx.x*D + 2);
    }             
    __syncthreads();
    
    __shared__ uint dist_X[BLOCK_X_SIZE]; //, dist_Y[BLOCK_Y_SIZE];    
    // initialize to maximum UINT value (for minumin distance computation)
    dist_X[threadIdx.x] = UINT_MAX;
    //dist_Y[threadIdx.x] = UINT_MAX;
    
    __syncthreads();
    
    //calculate the minimum distance: x to every point in Y
    if (ix<N && iy<M) {
        float n1, n2, n3, dd1, dd2, dd3;
        n1 = X[3*threadIdx.x];
        n2 = X[3*threadIdx.x+1];
        n3 = X[3*threadIdx.x+2];

        dd1 = r11*Y[3*threadIdx.y] + r12*Y[3*threadIdx.y+1] + r13*Y[3*threadIdx.y+2] + t1; // r11*m1 + r12*m2 + r13*m3 + t1;
        dd2 = r21*Y[3*threadIdx.y] + r22*Y[3*threadIdx.y+1] + r23*Y[3*threadIdx.y+2] + t2; // r21*m1 + r22*m2 + r23*m3 + t2;
        dd3 = r31*Y[3*threadIdx.y] + r32*Y[3*threadIdx.y+1] + r33*Y[3*threadIdx.y+2] + t3; //r31*m1 + r32*m2 + r33*m3 + t3;

        float s = ((n1 - dd1)*(n1 - dd1) + (n2 - dd2)*(n2 - dd2) + (n3 - dd3)*(n3 - dd3)) / (float)D * SCALE_FACTOR;
        // avoid the race condition
        atomicMin(&dist_X[threadIdx.x], __float2uint_rn(s));
    }
    
    __syncthreads();  
    if ((ix < N) && (iy < M) && (threadIdx.y==0))
        atomicMin(distance_X + ix, dist_X[threadIdx.x]);
    __syncthreads(); 
    
    return;
}


__global__ void distance_reduction_kernel(float * B_C,int n_c, int L,int stride) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    if (idx<(L+1) / 2) {
        if (((L & 1) == 0) || (idx != (L + 1)/2 - 1)) {
            for (int i = 0; i < n_c; i++)
                atomicAdd((B_C + idx*2*stride + i), *(B_C + idx*2*stride + stride + i));
        }
    } 
    __syncthreads();
}


// d_idata is the array waiting to sum
// coefficient is the final sum whose number is n_c
// size is the length of the array
void compute_distance_reduction(float *d_idata, float *coefficient, int size, int n_c) {
    int t = size / n_c;
    int stride = n_c;
    int blocksize = 1024;
    dim3 block(blocksize,1);
    while (t>1) {
        // there are t numbers to deal with
        dim3 grid(((t + 1)/2 + block.x - 1) / block.x, 1);
        // invoke the kernel
        distance_reduction_kernel<<<grid, block>>>(d_idata, n_c, t, stride);
        cudaDeviceSynchronize();
        //update the state
        t = (t + 1) / 2;
        stride = stride*2;
    }

    cudaMemcpy(coefficient, d_idata, n_c*sizeof(float), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
}


__global__ void convert_back(uint *distance_X, float * distance_XX) {
    int ix = blockIdx.x*blockDim.x + threadIdx.x;
    if (ix < d_Data_Coef[0]) { // < N
        *(distance_XX + ix) = __uint2float_rn(*(distance_X + ix)) / SCALE_FACTOR;
    }
}


