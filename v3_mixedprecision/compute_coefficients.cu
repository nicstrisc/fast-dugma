#include <cuda_runtime.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <cuda_fp16.h>

#define THREAD_POOL_COEFFICIENTS 8

#define MAX_POINT_NUMBER 1000000

#define MAX_SIZE_DATA 3
#define MAX_SIZE_TRAN 12
#define BLOCK_X_SIZE 32
#define BLOCK_Y_SIZE 32

#define times2D 1      // the coefficient matrix in shared memory should be 46*32*times2D
#define times3D 1      // the coefficient matrix in shared memory should be 4*637*times3D


__constant__ int d_Data_Coef[MAX_SIZE_DATA]; //include N,M,D
__constant__ double d_Tran_Coef[MAX_SIZE_TRAN];
__constant__ double d_Sig_Coef[1];

/**
 * @brief Compute the coefficients of the optimization function
 * @param data      MEX parameter matrix
 * @param R         Rotation matrix
 * @param t         Translation vector
 * @param N1        Size of the 1st pointcloud
 * @param M1        Size of the 2nd pointcloud
 * @param D1        Number of dimensions (2 or 3) of the point space
 * @param sig1      Sigma
 * @param cMatrix   Output coefficient matrix
 */
void compute_coefficients(float *data[6], double *R, double *t, double * N, double * M, double * D, double * sig1, double cMatrix[640]);

/**
 * @brief Compute the coefficients of the optimization for 2D point clouds (CUDA kernel)
 * @param d_X       First point cloud
 * @param d_X_C     Uncertainty first point cloud
 * @param det_X     Determinants first point cloud
 * @param d_Y       Second point cloud
 * @param d_Y_C     Uncertainty second point cloud
 * @param det_Y     Determinants second point cloud
 * @param B_C       Result matrix
 */
__global__ void coef2D(float *d_X, float *d_X_C, float *det_X, float *d_Y, float *d_Y_C, float *det_Y, double *B_C);

/**
 * @brief Compute the coefficients of the optimization for 3D point clouds (CUDA kernel)
 * @param d_X       First point cloud
 * @param d_X_C     Uncertainty first point cloud
 * @param det_X     Determinants first point cloud
 * @param d_Y       Second point cloud
 * @param d_Y_C     Uncertainty second point cloud
 * @param det_Y     Determinants second point cloud
 * @param B_C       Result matrix
 */
__global__ void coef3D(float *d_X, float *d_X_C, float *det_X, float *d_Y, float *d_Y_C, float *det_Y, double *B_C);

/**
 * @brief Reduce the partial coefficient results to a single matrix
 * @param d_idata
 * @param coefficient
 * @param size
 * @param n_c
 */
void coeff_reduction(double *d_idata, double *coefficient, int size, int n_c, float norm_factor);

__global__ void coeff_reduction_kernel(double * B_C,int n_c, int L,int stride);

/**
 * @brief set_constants Set constant parameter values into GPU global memory
 * @param N
 * @param M
 * @param D
 * @param sig
 * @param R
 * @param t
 */
void set_constants(int N, int M, int D, double sig, double *R, double *t);

void check(cudaError_t error_id) {
    if (error_id != cudaSuccess) {
        printf("cudaGetDeviceCount returned %d\n-> %s\n",(int)error_id,cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }
}

__forceinline__ __device__ unsigned dynamic_smem_size() {
    unsigned ret;
    asm volatile ("mov.u32 %0, %dynamic_smem_size;" : "=r"(ret));
    return ret;
}



void compute_coefficients(float *data[6], double *R, double *t, double * N1, double * M1, double * D1, double * sig1, double cMatrix[640]) {
    float *d_X, *d_X_C, *d_Y, *d_Y_C, *det_X, *det_Y;
    d_X     = data[0];
    d_X_C   = data[1];
    d_Y     = data[2];
    d_Y_C   = data[3];
    det_X   = data[4];
    det_Y   = data[5];

    int M, N, D;
    N = static_cast<int>(*N1);
    M = static_cast<int>(*M1);
    D = static_cast<int>(*D1);

    set_constants(N, M, D, *sig1, R, t);

    if (M > MAX_POINT_NUMBER || N > MAX_POINT_NUMBER) {
        printf("ERROR: number of points in the pointclouds is too high.");
        exit(EXIT_FAILURE);
    }

    dim3 threadsPerBlock(BLOCK_X_SIZE, BLOCK_Y_SIZE, 1);
    dim3 blocks((N + threadsPerBlock.x - 1) / threadsPerBlock.x,
                (M + threadsPerBlock.y - 1) / threadsPerBlock.y);

    // malloc device global memory for coefficient
    // for 2D, we also use the block(32,32), coefficient matrix in shared memory will be [32*45]
    // the maximum number of coefficient matrixes(4*637 or 32*45) is 4096, 3D:80MB, 2D:46MB
    unsigned int nBytes;
    int NN = blocks.x;  //blocks in X direction
    int MM = blocks.y;  //blocks in Y direction

    int size, n_c;
    if (D == 2) {
        n_c = 46;
        if (NN*MM <= 4096) {
            nBytes = NN * MM * 32 * n_c * sizeof(double)*times2D;
            //the length of B_C
            size=NN*MM*32*n_c*times2D;
        }
        else
        {
            nBytes=4096*32*n_c*sizeof(double)*times2D;
            size=4096*32*n_c*times2D;
        }
    }
    else {
        n_c = 640;
        if (NN * MM <= 4096) {
            nBytes = NN * MM * THREAD_POOL_COEFFICIENTS * n_c * sizeof(double) * times3D; // might be sizeof(float)
            size = NN * MM * THREAD_POOL_COEFFICIENTS * n_c * times3D;
        }
        else {
            nBytes = 4096 * THREAD_POOL_COEFFICIENTS * n_c * sizeof(double) * times3D;
            size = 4096 * THREAD_POOL_COEFFICIENTS * n_c * times3D;
        }
    }

    double *B_C;  // the coefficient box in device global memory
    check(cudaMalloc((void **) &B_C, nBytes));
    check(cudaMemset(B_C, 0, nBytes));
    //check(cudaDeviceSynchronize());

    // malloc the memory for the coefficient
    double *coefficient = (double *) malloc(n_c * sizeof(double));
    memset(coefficient, 0, n_c * sizeof(double));

    if (D==2)
    {
        //coef2D
        coef2D<<<blocks, threadsPerBlock>>>(d_X,d_X_C,det_X,d_Y,d_Y_C,det_Y,B_C);
        check(cudaDeviceSynchronize());
        //reduction
        coeff_reduction(B_C,coefficient,size, n_c, 1 / (float) (N*M));
    }
    else {
        // compute coefficients
        coef3D<<<blocks, threadsPerBlock>>>(d_X, d_X_C, det_X, d_Y, d_Y_C, det_Y, B_C);
        // check memory
        //check(cudaDeviceSynchronize());
        // Reduction of the coefficient partial matrices into the total coefficient matrix
        coeff_reduction(B_C, coefficient, size, n_c, 1 / (float) (N*M));
	// --------------------------------------------------------------------------------------------------
	// In the computation of coefficients, many were computed more than once with the same formulas
	// In this version we compute unique coefficients once, and here we copy the final results into
	// the appropriate vector cells (where duplicate coefficients were computed)
	coefficient[2] = coefficient[1];
	coefficient[4] = coefficient[1];
	coefficient[5] = coefficient[3];
	coefficient[7] = coefficient[3];
	coefficient[8] = coefficient[6];
	coefficient[9] = coefficient[6];
	coefficient[33] = coefficient[28];
	coefficient[40] = coefficient[28];
	coefficient[34] = coefficient[29];
	coefficient[41] = coefficient[29];
	coefficient[32] = coefficient[30];
	coefficient[36] = coefficient[30];
	coefficient[38] = coefficient[31];
	coefficient[44] = coefficient[31];
	coefficient[37] = coefficient[35];
	coefficient[42] = coefficient[35];
	coefficient[43] = coefficient[39];
	coefficient[45] = coefficient[39];
	coefficient[176] = coefficient[46];
	coefficient[188] = coefficient[46];
	coefficient[177] = coefficient[47];
	coefficient[208] = coefficient[47];
	coefficient[196] = coefficient[48];
	coefficient[213] = coefficient[48];
	coefficient[189] = coefficient[49];
	coefficient[209] = coefficient[49];
	coefficient[199] = coefficient[50];
	coefficient[230] = coefficient[50];
	coefficient[225] = coefficient[51];
	coefficient[237] = coefficient[51];
	coefficient[217] = coefficient[52];
	coefficient[232] = coefficient[52];
	coefficient[227] = coefficient[53];
	coefficient[242] = coefficient[53];
	coefficient[239] = coefficient[54];
	coefficient[243] = coefficient[54];
	coefficient[79] = coefficient[73];
	coefficient[141] = coefficient[73];
	coefficient[132] = coefficient[73];
	coefficient[116] = coefficient[73];
	coefficient[111] = coefficient[73];
	coefficient[85] = coefficient[73];
	coefficient[117] = coefficient[73];
	coefficient[133] = coefficient[73];
	coefficient[80] = coefficient[74];
	coefficient[86] = coefficient[74];
	coefficient[112] = coefficient[74];
	coefficient[118] = coefficient[74];
	coefficient[119] = coefficient[74];
	coefficient[134] = coefficient[74];
	coefficient[135] = coefficient[74];
	coefficient[142] = coefficient[74];
	coefficient[81] = coefficient[75];
	coefficient[87] = coefficient[75];
	coefficient[113] = coefficient[75];
	coefficient[120] = coefficient[75];
	coefficient[121] = coefficient[75];
	coefficient[136] = coefficient[75];
	coefficient[137] = coefficient[75];
	coefficient[143] = coefficient[75];
	coefficient[82] = coefficient[76];
	coefficient[88] = coefficient[76];
	coefficient[115] = coefficient[76];
	coefficient[123] = coefficient[76];
	coefficient[124] = coefficient[76];
	coefficient[139] = coefficient[76];
	coefficient[140] = coefficient[76];
	coefficient[146] = coefficient[76];
	coefficient[83] = coefficient[77];
	coefficient[89] = coefficient[77];
	coefficient[122] = coefficient[77];
	coefficient[128] = coefficient[77];
	coefficient[129] = coefficient[77];
	coefficient[144] = coefficient[77];
	coefficient[145] = coefficient[77];
	coefficient[151] = coefficient[77];
	coefficient[84] = coefficient[78];
	coefficient[90] = coefficient[78];
	coefficient[125] = coefficient[78];
	coefficient[130] = coefficient[78];
	coefficient[131] = coefficient[78];
	coefficient[147] = coefficient[78];
	coefficient[148] = coefficient[78];
	coefficient[152] = coefficient[78];
	coefficient[92] = coefficient[91];
	coefficient[95] = coefficient[91];
	coefficient[96] = coefficient[91];
	coefficient[101] = coefficient[91];
	coefficient[102] = coefficient[91];
	coefficient[94] = coefficient[93];
	coefficient[97] = coefficient[93];
	coefficient[98] = coefficient[93];
	coefficient[103] = coefficient[93];
	coefficient[104] = coefficient[93];
	coefficient[100] = coefficient[99];
	coefficient[105] = coefficient[99];
	coefficient[106] = coefficient[99];
	coefficient[107] = coefficient[99];
	coefficient[108] = coefficient[99];
	coefficient[126] = coefficient[109];
	coefficient[149] = coefficient[109];
	coefficient[127] = coefficient[110];
	coefficient[150] = coefficient[110];
	coefficient[138] = coefficient[114];
	coefficient[153] = coefficient[114];
	coefficient[155] = coefficient[154];
	coefficient[157] = coefficient[156];
	coefficient[159] = coefficient[158];
	coefficient[161] = coefficient[160];
	coefficient[163] = coefficient[162];
	coefficient[165] = coefficient[164];
	coefficient[167] = coefficient[166];
	coefficient[169] = coefficient[168];
	coefficient[171] = coefficient[170];
	coefficient[190] = coefficient[178];
	coefficient[210] = coefficient[179];
	coefficient[192] = coefficient[180];
	coefficient[212] = coefficient[181];
	coefficient[195] = coefficient[182];
	coefficient[215] = coefficient[183];
	coefficient[201] = coefficient[184];
	coefficient[219] = coefficient[186];
	coefficient[211] = coefficient[193];
	coefficient[214] = coefficient[198];
	coefficient[216] = coefficient[200];
	coefficient[218] = coefficient[202];
	coefficient[233] = coefficient[204];
	coefficient[221] = coefficient[205];
	coefficient[222] = coefficient[206];
	coefficient[236] = coefficient[207];
	coefficient[235] = coefficient[223];
	coefficient[238] = coefficient[226];
	coefficient[247] = coefficient[244];
	coefficient[248] = coefficient[244];
	coefficient[253] = coefficient[244];
	coefficient[254] = coefficient[244];
	coefficient[259] = coefficient[244];
	coefficient[249] = coefficient[245];
	coefficient[250] = coefficient[245];
	coefficient[255] = coefficient[245];
	coefficient[256] = coefficient[245];
	coefficient[260] = coefficient[245];
	coefficient[251] = coefficient[246];
	coefficient[252] = coefficient[246];
	coefficient[257] = coefficient[246];
	coefficient[258] = coefficient[246];
	coefficient[261] = coefficient[246];
	coefficient[271] = coefficient[262];
	coefficient[280] = coefficient[262];
	coefficient[272] = coefficient[263];
	coefficient[281] = coefficient[263];
	coefficient[265] = coefficient[264];
	coefficient[273] = coefficient[264];
	coefficient[274] = coefficient[264];
	coefficient[282] = coefficient[264];
	coefficient[283] = coefficient[264];
	coefficient[267] = coefficient[266];
	coefficient[275] = coefficient[266];
	coefficient[276] = coefficient[266];
	coefficient[284] = coefficient[266];
	coefficient[285] = coefficient[266];
	coefficient[269] = coefficient[268];
	coefficient[277] = coefficient[268];
	coefficient[278] = coefficient[268];
	coefficient[286] = coefficient[268];
	coefficient[287] = coefficient[268];
	coefficient[279] = coefficient[270];
	coefficient[288] = coefficient[270];
	coefficient[332] = coefficient[290];
	coefficient[333] = coefficient[291];
	coefficient[334] = coefficient[292];
	coefficient[336] = coefficient[293];
	coefficient[335] = coefficient[294];
	coefficient[337] = coefficient[295];
	coefficient[339] = coefficient[296];
	coefficient[318] = coefficient[297];
	coefficient[319] = coefficient[298];
	coefficient[320] = coefficient[299];
	coefficient[321] = coefficient[300];
	coefficient[322] = coefficient[301];
	coefficient[323] = coefficient[302];
	coefficient[324] = coefficient[303];
	coefficient[325] = coefficient[304];
	coefficient[326] = coefficient[305];
	coefficient[327] = coefficient[306];
	coefficient[328] = coefficient[307];
	coefficient[329] = coefficient[308];
	coefficient[330] = coefficient[309];
	coefficient[344] = coefficient[310];
	coefficient[331] = coefficient[311];
	coefficient[345] = coefficient[312];
	coefficient[338] = coefficient[314];
	coefficient[340] = coefficient[315];
	coefficient[341] = coefficient[316];
	coefficient[342] = coefficient[317];
	coefficient[356] = coefficient[355];
	coefficient[358] = coefficient[355];
	coefficient[359] = coefficient[357];
	coefficient[361] = coefficient[357];
	coefficient[362] = coefficient[360];
	coefficient[363] = coefficient[360];
	coefficient[373] = coefficient[366];
	coefficient[274] = coefficient[368];
	coefficient[376] = coefficient[369];
	coefficient[375] = coefficient[370];
	coefficient[378] = coefficient[371];
	coefficient[381] = coefficient[372];
	coefficient[385] = coefficient[380];
	coefficient[386] = coefficient[383];
	coefficient[387] = coefficient[384];
	coefficient[392] = coefficient[391];
	coefficient[394] = coefficient[391];
	coefficient[512] = coefficient[391];
	coefficient[513] = coefficient[391];
	coefficient[524] = coefficient[391];
	coefficient[525] = coefficient[391];
	coefficient[544] = coefficient[391];
	coefficient[545] = coefficient[391];
	coefficient[395] = coefficient[393];
	coefficient[397] = coefficient[393];
	coefficient[532] = coefficient[393];
	coefficient[535] = coefficient[393];
	coefficient[549] = coefficient[393];
	coefficient[553] = coefficient[393];
	coefficient[566] = coefficient[393];
	coefficient[568] = coefficient[393];
	coefficient[398] = coefficient[396];
	coefficient[399] = coefficient[396];
	coefficient[561] = coefficient[396];
	coefficient[563] = coefficient[396];
	coefficient[573] = coefficient[396];
	coefficient[575] = coefficient[396];
	coefficient[578] = coefficient[396];
	coefficient[579] = coefficient[396];
	coefficient[420] = coefficient[400];
	coefficient[431] = coefficient[400];
	coefficient[421] = coefficient[401];
	coefficient[449] = coefficient[401];
	coefficient[438] = coefficient[402];
	coefficient[455] = coefficient[402];
	coefficient[432] = coefficient[403];
	coefficient[450] = coefficient[403];
	coefficient[439] = coefficient[404];
	coefficient[469] = coefficient[404];
	coefficient[466] = coefficient[405];
	coefficient[476] = coefficient[405];
	coefficient[456] = coefficient[406];
	coefficient[470] = coefficient[406];
	coefficient[467] = coefficient[407];
	coefficient[479] = coefficient[407];
	coefficient[477] = coefficient[408];
	coefficient[480] = coefficient[408];
	coefficient[412] = coefficient[409];
	coefficient[415] = coefficient[409];
	coefficient[482] = coefficient[409];
	coefficient[483] = coefficient[409];
	coefficient[487] = coefficient[409];
	coefficient[488] = coefficient[409];
	coefficient[495] = coefficient[409];
	coefficient[496] = coefficient[409];
	coefficient[413] = coefficient[410];
	coefficient[416] = coefficient[410];
	coefficient[484] = coefficient[410];
	coefficient[485] = coefficient[410];
	coefficient[490] = coefficient[410];
	coefficient[491] = coefficient[410];
	coefficient[498] = coefficient[410];
	coefficient[499] = coefficient[410];
	coefficient[414] = coefficient[411];
	coefficient[417] = coefficient[411];
	coefficient[492] = coefficient[411];
	coefficient[493] = coefficient[411];
	coefficient[500] = coefficient[411];
	coefficient[501] = coefficient[411];
	coefficient[504] = coefficient[411];
	coefficient[505] = coefficient[411];
	coefficient[423] = coefficient[422];
	coefficient[433] = coefficient[422];
	coefficient[434] = coefficient[422];
	coefficient[425] = coefficient[424];
	coefficient[451] = coefficient[424];
	coefficient[452] = coefficient[424];
	coefficient[427] = coefficient[426];
	coefficient[440] = coefficient[426];
	coefficient[441] = coefficient[426];
	coefficient[429] = coefficient[428];
	coefficient[457] = coefficient[428];
	coefficient[458] = coefficient[428];
	coefficient[437] = coefficient[436];
	coefficient[453] = coefficient[436];
	coefficient[454] = coefficient[436];
	coefficient[444] = coefficient[443];
	coefficient[459] = coefficient[443];
	coefficient[460] = coefficient[443];
	coefficient[446] = coefficient[445];
	coefficient[461] = coefficient[445];
	coefficient[462] = coefficient[445];
	coefficient[448] = coefficient[447];
	coefficient[472] = coefficient[447];
	coefficient[473] = coefficient[447];
	coefficient[465] = coefficient[464];
	coefficient[474] = coefficient[464];
	coefficient[475] = coefficient[464];
	coefficient[486] = coefficient[481];
	coefficient[494] = coefficient[481];
	coefficient[497] = coefficient[489];
	coefficient[503] = coefficient[489];
	coefficient[506] = coefficient[502];
	coefficient[507] = coefficient[502];
	coefficient[527] = coefficient[508];
	coefficient[564] = coefficient[508];
	coefficient[530] = coefficient[509];
	coefficient[565] = coefficient[509];
	coefficient[533] = coefficient[510];
	coefficient[567] = coefficient[510];
	coefficient[539] = coefficient[511];
	coefficient[570] = coefficient[511];
	coefficient[515] = coefficient[514];
	coefficient[526] = coefficient[514];
	coefficient[529] = coefficient[514];
	coefficient[546] = coefficient[514];
	coefficient[547] = coefficient[514];
	coefficient[517] = coefficient[516];
	coefficient[528] = coefficient[516];
	coefficient[534] = coefficient[516];
	coefficient[548] = coefficient[516];
	coefficient[550] = coefficient[516];
	coefficient[519] = coefficient[518];
	coefficient[531] = coefficient[518];
	coefficient[536] = coefficient[518];
	coefficient[551] = coefficient[518];
	coefficient[552] = coefficient[518];
	coefficient[522] = coefficient[520];
	coefficient[537] = coefficient[520];
	coefficient[542] = coefficient[520];
	coefficient[555] = coefficient[520];
	coefficient[558] = coefficient[520];
	coefficient[556] = coefficient[521];
	coefficient[576] = coefficient[521];
	coefficient[560] = coefficient[523];
	coefficient[577] = coefficient[523];
	coefficient[540] = coefficient[538];
	coefficient[554] = coefficient[538];
	coefficient[559] = coefficient[538];
	coefficient[569] = coefficient[538];
	coefficient[571] = coefficient[538];
	coefficient[543] = coefficient[541];
	coefficient[557] = coefficient[541];
	coefficient[562] = coefficient[541];
	coefficient[572] = coefficient[541];
	coefficient[574] = coefficient[541];
	coefficient[604] = coefficient[580];
	coefficient[634] = coefficient[580];
	coefficient[588] = coefficient[581];
	coefficient[589] = coefficient[581];
	coefficient[609] = coefficient[581];
	coefficient[610] = coefficient[581];
	coefficient[623] = coefficient[581];
	coefficient[590] = coefficient[582];
	coefficient[592] = coefficient[582];
	coefficient[611] = coefficient[582];
	coefficient[613] = coefficient[582];
	coefficient[624] = coefficient[582];
	coefficient[591] = coefficient[583];
	coefficient[593] = coefficient[583];
	coefficient[612] = coefficient[583];
	coefficient[614] = coefficient[583];
	coefficient[625] = coefficient[583];
	coefficient[595] = coefficient[584];
	coefficient[597] = coefficient[584];
	coefficient[616] = coefficient[584];
	coefficient[618] = coefficient[584];
	coefficient[627] = coefficient[584];
	coefficient[594] = coefficient[585];
	coefficient[598] = coefficient[585];
	coefficient[615] = coefficient[585];
	coefficient[619] = coefficient[585];
	coefficient[626] = coefficient[585];
	coefficient[596] = coefficient[586];
	coefficient[599] = coefficient[586];
	coefficient[617] = coefficient[586];
	coefficient[620] = coefficient[586];
	coefficient[628] = coefficient[586];
	coefficient[600] = coefficient[587];
	coefficient[602] = coefficient[587];
	coefficient[621] = coefficient[587];
	coefficient[622] = coefficient[587];
	coefficient[630] = coefficient[587];
	coefficient[605] = coefficient[601];
	coefficient[606] = coefficient[601];
	coefficient[629] = coefficient[601];
	coefficient[631] = coefficient[601];
	coefficient[635] = coefficient[601];
	coefficient[607] = coefficient[603];
	coefficient[608] = coefficient[603];
	coefficient[632] = coefficient[603];
	coefficient[633] = coefficient[603];
	coefficient[636] = coefficient[603];
	// --------------------------------------------------------------------------------------------------
    }

    //check(cudaDeviceSynchronize());

    for (int i = 0; i < n_c; i++)
        cMatrix[i] = *(coefficient + i);

    //check(cudaDeviceSynchronize());

    free(coefficient);
    check(cudaFree(B_C));
    return;
}



void set_constants(int N, int M, int D, double sig, double *R, double *t) {
    const int h_Data_Coef[MAX_SIZE_DATA] = {N,M,D};
    const double h_Sig_Coef[1] = {sig};

    // copy data on host to the constant memory on device
    check(cudaMemcpyToSymbol(d_Sig_Coef, h_Sig_Coef, sizeof(double)));
    check(cudaMemcpyToSymbol(d_Data_Coef, h_Data_Coef, MAX_SIZE_DATA * sizeof(int)));

    check(cudaDeviceSynchronize());

    double h_Tran_Coef[MAX_SIZE_TRAN];
    if (D==2)
    {
        for (int i=0;i<4;i++)
        {
            h_Tran_Coef[i]=*(R+i);
        }
        for (int i=0;i<2;i++)
        {
            h_Tran_Coef[i+4]=*(t+i);
        }
        for (int i=0;i<6;i++)
        {
            h_Tran_Coef[i+6]=0;
        }
    }
    else {
        for (int i=0; i < 9; i++)
            h_Tran_Coef[i] = *(R + i);

        for (int i=0; i < 3; i++)
            h_Tran_Coef[i + 9] = *(t + i);

    }
    check(cudaMemcpyToSymbol(d_Tran_Coef, h_Tran_Coef, MAX_SIZE_TRAN * sizeof(double)));
    check(cudaDeviceSynchronize());
}


__global__ void coef2D(float *d_X,float *d_X_C,float *det_X,float *d_Y,float *d_Y_C,float *det_Y,double *B_C) {
    printf("coef2D not implemented");
    return;
}


__global__ void coef3D(float *d_X, float *d_X_C, float *det_X,
                       float *d_Y, float *d_Y_C, float *det_Y,
                       double *B_C) {
    int n_c = 640;
    int ix, iy, N, M, D;
    ix = blockIdx.x * blockDim.x + threadIdx.x;
    iy = blockIdx.y * blockDim.y + threadIdx.y;

    N = d_Data_Coef[0];
    M = d_Data_Coef[1];
    D = d_Data_Coef[2];

    double sig;
    sig=d_Sig_Coef[0];
    sig=1/sig;
    float Isig;
    Isig=__double2float_rn(sig);

    __shared__ float X[96], X_C[288], detX[32], Y[96], Y_C[288], detY[32];
    // put the data related to x,y into the shared memory
    if ((threadIdx.y == 0) && (ix < N)) {
        X[threadIdx.x*D]=*(d_X+ix*D);
        X[threadIdx.x*D+1]=*(d_X+ix*D+1);
        X[threadIdx.x*D+2]=*(d_X+ix*D+2);
        for (int ii=0; ii < 9; ii++)
            X_C[threadIdx.x*D*D+ii]=*(d_X_C+ix*D*D+ii)*Isig;
        detX[threadIdx.x]=*(det_X+ix)*sqrtf(Isig*Isig*Isig);
    }

    //__syncthreads(); //v4
    if ((threadIdx.y == 0)  && (blockIdx.y * blockDim.y + threadIdx.x < M)) {
        Y[threadIdx.x*D]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D);
        Y[threadIdx.x*D+1]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+1);
        Y[threadIdx.x*D+2]=*(d_Y+blockIdx.y*blockDim.y*D+threadIdx.x*D+2);
        for (int ii = 0; ii < 9; ii++)
            Y_C[threadIdx.x*D*D+ii] = *(d_Y_C+blockIdx.y*blockDim.y*D*D+threadIdx.x*D*D+ii)*Isig;
        detY[threadIdx.x]=*(det_Y+blockIdx.y*blockDim.y+threadIdx.x)*sqrtf(Isig*Isig*Isig);
    }
    __syncthreads();

    //__shared__ float mat[THREAD_POOL_COEFFICIENTS * times3D][640];
    __shared__ __half mat[THREAD_POOL_COEFFICIENTS * times3D][640];

    // initilize mat to zero
    if (threadIdx.y < THREAD_POOL_COEFFICIENTS)
        for (int ii = 0; ii < 20; ii++)
            mat[threadIdx.y][threadIdx.x * 20 + ii] = 0; //__float2half(0);

    __syncthreads();

    //calculate the coefficient
    if (ix < N && iy < M) {
        float n1,n2,n3,m1,m2,m3;
        float a11,a12,a13,a21,a22,a23,a31,a32,a33;


        float b11,b12,b13,b21,b22,b23,b31,b32,b33;

        n1=X[3*threadIdx.x];
        n2=X[3*threadIdx.x+1];
        n3=X[3*threadIdx.x+2];
        m1=Y[3*threadIdx.y];
        m2=Y[3*threadIdx.y+1];
        m3=Y[3*threadIdx.y+2];
        // A is inv(Cy)
        // B is inv(Cx)
        a11=Y_C[9*threadIdx.y];
        a21=Y_C[9*threadIdx.y+1];
        a31=Y_C[9*threadIdx.y+2];
        a12=Y_C[9*threadIdx.y+3];
        a22=Y_C[9*threadIdx.y+4];
        a32=Y_C[9*threadIdx.y+5];
        a13=Y_C[9*threadIdx.y+6];
        a23=Y_C[9*threadIdx.y+7];
        a33=Y_C[9*threadIdx.y+8];

        b11=X_C[9*threadIdx.x];
        b21=X_C[9*threadIdx.x+1];
        b31=X_C[9*threadIdx.x+2];
        b12=X_C[9*threadIdx.x+3];
        b22=X_C[9*threadIdx.x+4];
        b32=X_C[9*threadIdx.x+5];
        b13=X_C[9*threadIdx.x+6];
        b23=X_C[9*threadIdx.x+7];
        b33=X_C[9*threadIdx.x+8];

        float r11,r12,r13,r21,r22,r23,r31,r32,r33,t1,t2,t3; // the old R,t
        r11=d_Tran_Coef[0];
        r21=d_Tran_Coef[1];
        r31=d_Tran_Coef[2];
        r12=d_Tran_Coef[3];
        r22=d_Tran_Coef[4];
        r32=d_Tran_Coef[5];
        r13=d_Tran_Coef[6];
        r23=d_Tran_Coef[7];
        r33=d_Tran_Coef[8];

        t1=d_Tran_Coef[9];
        t2=d_Tran_Coef[10];
        t3=d_Tran_Coef[11];

        // calculate P
        float dd1,dd2,dd3;
        float c11,c12,c13,c21,c22,c23,c31,c32,c33,mal1,mal2;

        //calculate the P
        dd1 = n1 - t1 - m1*r11 - m2*r12 - m3*r13;
        dd2 = n2 - t2 - m1*r21 - m2*r22 - m3*r23;
        dd3 = n3 - t3 - m1*r31 - m2*r32 - m3*r33;

        // C = R*inv(Cy)*R'
        c11 = r11*(a11*r11 + a21*r12 + a31*r13) + r12*(a12*r11 + a22*r12 + a32*r13) + r13*(a13*r11 + a23*r12 + a33*r13);
        c12 = r11*(a11*r21 + a21*r22 + a31*r23) + r12*(a12*r21 + a22*r22 + a32*r23) + r13*(a13*r21 + a23*r22 + a33*r23);
        c13 = r11*(a11*r31 + a21*r32 + a31*r33) + r12*(a12*r31 + a22*r32 + a32*r33) + r13*(a13*r31 + a23*r32 + a33*r33);
        c21 = r21*(a11*r11 + a21*r12 + a31*r13) + r22*(a12*r11 + a22*r12 + a32*r13) + r23*(a13*r11 + a23*r12 + a33*r13);
        c22 = r21*(a11*r21 + a21*r22 + a31*r23) + r22*(a12*r21 + a22*r22 + a32*r23) + r23*(a13*r21 + a23*r22 + a33*r23);
        c23 = r21*(a11*r31 + a21*r32 + a31*r33) + r22*(a12*r31 + a22*r32 + a32*r33) + r23*(a13*r31 + a23*r32 + a33*r33);
        c31 = r31*(a11*r11 + a21*r12 + a31*r13) + r32*(a12*r11 + a22*r12 + a32*r13) + r33*(a13*r11 + a23*r12 + a33*r13);
        c32 = r31*(a11*r21 + a21*r22 + a31*r23) + r32*(a12*r21 + a22*r22 + a32*r23) + r33*(a13*r21 + a23*r22 + a33*r23);
        c33 = r31*(a11*r31 + a21*r32 + a31*r33) + r32*(a12*r31 + a22*r32 + a32*r33) + r33*(a13*r31 + a23*r32 + a33*r33);

        // calculate (xn-ym)'*inv(R*Cy*R')*(xn-ym); (xn-ym)'*inv(Cx)*(xn-ym)
        mal1=dd1*(c11*dd1 + c21*dd2 + c31*dd3) + dd2*(c12*dd1 + c22*dd2 + c32*dd3) + dd3*(c13*dd1 + c23*dd2 + c33*dd3);
        mal2=dd1*(b11*dd1 + b21*dd2 + b31*dd3) + dd2*(b12*dd1 + b22*dd2 + b32*dd3) + dd3*(b13*dd1 + b23*dd2 + b33*dd3);

        //
        float Pr = detX[threadIdx.x] * detY[threadIdx.y] * (exp(-0.5*mal1) + exp(-0.5*mal2)) / (N*M); // adding a Prob. normalization factor (N*M)
        if (Pr != 0.0) {
            __half P = __float2half(Pr);
            // update the ym to calculate the P
            dd1=r11*m1+r12*m2+r13*m3+t1;  // the new ym_x
            dd2=r21*m1+r22*m2+r23*m3+t2;  // the new ym_y
            dd3=r31*m1+r32*m2+r33*m3+t3;

            m1 = dd1;
            m2 = dd2;
            m3 = dd3;

            __half ha11 = __float2half(c11);
            __half ha12 = __float2half(c12);
            __half ha13 = __float2half(c13);
            __half ha21 = __float2half(c21);
            __half ha22 = __float2half(c22);
            __half ha23 = __float2half(c23);
            __half ha31 = __float2half(c31);
            __half ha32 = __float2half(c32);
            __half ha33 = __float2half(c33);

            __half hb11 = __float2half(b11);
            __half hb12 = __float2half(b12);
            __half hb13 = __float2half(b13);
            __half hb21 = __float2half(b21);
            __half hb22 = __float2half(b22);
            __half hb23 = __float2half(b23);
            __half hb31 = __float2half(b31);
            __half hb32 = __float2half(b32);
            __half hb33 = __float2half(b33);

            /*__half h_2a11 = __hmul(ha11, h2);
            __half h_2a12 = __hmul(ha12, h2);
            __half h_2a13 = __hmul(ha13, h2);
            __half h_2a21 = __hmul(ha21, h2);
            __half h_2a22 = __hmul(ha22, h2);
            __half h_2a23 = __hmul(ha23, h2);
            __half h_2a31 = __hmul(ha31, h2);
            __half h_2a32 = __hmul(ha32, h2);
            __half h_2a33 = __hmul(ha33, h2);*/

            __half h_2a11 = __float2half(c11 * 2.0);
            __half h_2a12 = __float2half(c12 * 2.0);
            __half h_2a13 = __float2half(c13 * 2.0);
            __half h_2a21 = __float2half(c21 * 2.0);
            __half h_2a22 = __float2half(c22 * 2.0);
            __half h_2a23 = __float2half(c23 * 2.0);
            __half h_2a31 = __float2half(c31 * 2.0);
            __half h_2a32 = __float2half(c32 * 2.0);
            __half h_2a33 = __float2half(c33 * 2.0);

            __half h_2b11 = __float2half(b11 * 2.0);
            //__half h_2b12 = __hmul(h2, hb12);
            //__half h_2b13 = __hmul(h2, hb13);
            //__half h_2b21 = __hmul(h2, hb21);
            __half h_2b22 = __float2half(b22 * 2.0);
            //__half h_2b23 = __hmul(h2, hb23);
            //__half h_2b31 = __hmul(h2, hb31);
            //__half h_2b32 = __hmul(h2, hb32);
            __half h_2b33 = __float2half(b33 * 2.0);

            __half hn1 = __float2half(n1);
            __half hn2 = __float2half(n2);
            __half hn3 = __float2half(n3);
            __half hm1 = __float2half(m1);
            __half hm2 = __float2half(m2);
            __half hm3 = __float2half(m3);


            __half n1n1 = __hmul(hn1, hn1);
            __half n2n2 = __hmul(hn2, hn2);
            __half n3n3 = __hmul(hn3, hn3);
            __half n1n2 = __hmul(hn1, hn2);
            __half n1n3 = __hmul(hn1, hn3);
            __half n2n3 = __hmul(hn2, hn3);

            __half m1m1 = __hmul(hm1, hm1);
            __half m1m2 = __hmul(hm1, hm2);
            __half m1m3 = __hmul(hm1, hm3);
            __half m2m2 = __hmul(hm2, hm2);
            __half m2m3 = __hmul(hm2, hm3);
            __half m3m3 = __hmul(hm3, hm3);

            __half m1n1 = __hmul(hm1, hn1);
            __half m1n2 = __hmul(hm1, hn2);
            __half m1n3 = __hmul(hm1, hn3);
            __half m2n1 = __hmul(hm2, hn1);
            __half m2n2 = __hmul(hm2, hn2);
            __half m2n3 = __hmul(hm2, hn3);
            __half m3n1 = __hmul(hm3, hn1);
            __half m3n2 = __hmul(hm3, hn2);
            __half m3n3 = __hmul(hm3, hn3);

            //calculate the coefficient;
            int i;
            int j = threadIdx.x & (THREAD_POOL_COEFFICIENTS - 1);
            __half temp;

            i=0;
            temp = __hadd(__hadd(__hadd(__hmul(hb11, n1n1), __hmul(hb22, n2n2)), __hadd(__hmul(hb33, n3n3), __hmul(hb12, n1n2))), __hadd(__hadd(__hadd(__hmul(hb13, n1n3), __hmul(hb21, n1n2)), __hadd(__hmul(hb23, n2n3), __hmul(hb31, n1n3))), __hmul(hb32, n2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=1;
            temp = __hmul(ha11, m1m1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=3;
            temp = __hmul(ha22, m2m2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=6;
            temp = __hmul(ha33, m3m3);
            atomicAdd(&mat[j][i], __hmul(P, temp));


            i=10;
            temp = __hadd(__hmul(ha11, n1n1), __hmul(hb11, m1m1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=11;
            temp = __hadd(__hmul(hb11, m2m2), __hmul(ha22, n1n1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=12;
            temp = __hadd(__hmul(hb11, m3m3), __hmul(ha33, n1n1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=13;
            temp = __hadd(__hmul(ha11, n2n2), __hmul(hb22, m1m1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=14;
            temp = __hadd(__hmul(ha11, n3n3), __hmul(hb33, m1m1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=15;
            temp = __hadd(__hmul(ha22, n2n2), __hmul(hb22, m2m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=16;
            temp = __hadd(__hmul(hb22, m3m3), __hmul(ha33, n2n2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=17;
            temp = __hadd(__hmul(ha22, n3n3), __hmul(hb33, m2m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=18;
            temp = __hadd(__hmul(ha33, n3n3), __hmul(hb33, m3m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=19;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2b11, m1n1), __hmul(hb12, m1n2)), __hmul(hb13, m1n3)), __hadd(__hmul(hb21, m1n2), __hmul(hb31, m1n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=20;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2b11, m2n1), __hmul(hb12, m2n2)), __hmul(hb13, m2n3)), __hadd(__hmul(hb21, m2n2), __hmul(hb31, m2n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=21;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2b11, m3n1), __hmul(hb12, m3n2)), __hmul(hb13, m3n3)), __hadd(__hmul(hb21, m3n2), __hmul(hb31, m3n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=22;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb12, m1n1), __hmul(hb21, m1n1)), __hmul(h_2b22, m1n2)), __hadd(__hmul(hb23, m1n3), __hmul(hb32, m1n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=23;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb12, m2n1), __hmul(hb21, m2n1)), __hmul(h_2b22, m2n2)), __hadd(__hmul(hb23, m2n3), __hmul(hb32, m2n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=24;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb12, m3n1), __hmul(hb21, m3n1)), __hmul(h_2b22, m3n2)), __hadd(__hmul(hb23, m3n3), __hmul(hb32, m3n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=25;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb13, m1n1), __hmul(hb23, m1n2)), __hmul(hb31, m1n1)), __hadd(__hmul(hb32, m1n2), __hmul(h_2b33, m1n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=26;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb13, m2n1), __hmul(hb23, m2n2)), __hmul(hb31, m2n1)), __hadd(__hmul(hb32, m2n2), __hmul(h_2b33, m2n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=27;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb13, m3n1), __hmul(hb23, m3n2)), __hmul(hb31, m3n1)), __hadd(__hmul(hb32, m3n2), __hmul(h_2b33, m3n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=28;
            temp = __hadd(__hadd(__hmul(ha11, m2m2), __hmul(ha22, m1m1)), __hadd(__hmul(h_2a12, m1m2), __hmul(h_2a21, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=29;
            temp = __hadd(__hadd(__hmul(ha11, m3m3), __hmul(ha33, m1m1)), __hadd(__hmul(h_2a13, m1m3), __hmul(h_2a31, m1m3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=30;
            temp = __hmul(h_2a11, m1m1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=31;
            temp = __hadd(__hadd(__hmul(ha22, m3m3), __hmul(ha33, m2m2)), __hadd(__hmul(h_2a23, m2m3), __hmul(h_2a32, m2m3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=35;
            temp = __hmul(h_2a22, m2m2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=39;
            temp = __hmul(h_2a33, m3m3);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=46;
            temp = __hneg(__hmul(h_2a11, m1n1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=47;
            temp = __hneg(__hmul(h_2a11, m1n2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=48;
            temp = __hneg(__hmul(h_2a22, m2n1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=49;
            temp = __hneg(__hmul(h_2a11, m1n3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=50;
            temp = __hneg(__hmul(h_2a22, m2n2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=51;
            temp = __hneg(__hmul(h_2a33, m3n1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=52;
            temp = __hneg(__hmul(h_2a22, m2n3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=53;
            temp = __hneg(__hmul(h_2a33, m3n2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=54;
            temp = __hneg(__hmul(h_2a33, m3n3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=55;
            temp = __hadd(__hadd(__hmul(ha12, n1n1), __hmul(ha21, n1n1)), __hmul(h_2b11, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=56;
            temp = __hadd(__hadd(__hmul(ha13, n1n1), __hmul(ha31, n1n1)), __hmul(h_2b11, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=57;
            temp = __hadd(__hadd(__hmul(hb12, m1m1), __hmul(hb21, m1m1)), __hmul(__hmul(h_2a11, hn1), hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=58;
            temp = __hadd(__hadd(__hmul(hb12, m2m2), __hmul(hb21, m2m2)), __hmul(__hmul(h_2a22, hn1), hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=59;
            temp = __hadd(__hadd(__hmul(ha23, n1n1), __hmul(ha32, n1n1)), __hmul(h_2b11, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=60;
            temp = __hadd(__hadd(__hmul(hb12, m3m3), __hmul(hb21, m3m3)), __hmul(__hmul(h_2a33, hn1), hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=61;
            temp = __hadd(__hadd(__hmul(hb13, m1m1), __hmul(hb31, m1m1)), __hmul(__hmul(h_2a11, hn1), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=62;
            temp = __hadd(__hadd(__hmul(ha12, n2n2), __hmul(ha21, n2n2)), __hmul(h_2b22, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=63;
            temp = __hadd(__hadd(__hmul(ha13, n2n2), __hmul(ha31, n2n2)), __hmul(h_2b22, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=64;
            temp = __hadd(__hadd(__hmul(hb13, m2m2), __hmul(hb31, m2m2)), __hmul(__hmul(h_2a22, hn1), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=65;
            temp = __hadd(__hadd(__hmul(hb13, m3m3), __hmul(hb31, m3m3)), __hmul(__hmul(h_2a33, hn1), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=66;
            temp = __hadd(__hadd(__hmul(ha23, n2n2), __hmul(ha32, n2n2)), __hmul(h_2b22, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=67;
            temp = __hadd(__hadd(__hmul(hb23, m1m1), __hmul(hb32, m1m1)), __hmul(__hmul(h_2a11, hn2), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=68;
            temp = __hadd(__hadd(__hmul(ha12, n3n3), __hmul(ha21, n3n3)), __hmul(h_2b33, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=69;
            temp = __hadd(__hadd(__hmul(hb23, m2m2), __hmul(hb32, m2m2)), __hmul(__hmul(h_2a22, hn2), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=70;
            temp = __hadd(__hadd(__hmul(ha13, n3n3), __hmul(ha31, n3n3)), __hmul(h_2b33, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=71;
            temp = __hadd(__hadd(__hmul(hb23, m3m3), __hmul(hb32, m3m3)), __hmul(__hmul(h_2a33, hn2), hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=72;
            temp = __hadd(__hadd(__hmul(ha23, n3n3), __hmul(ha32, n3n3)), __hmul(h_2b33, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=73;
            temp = __hadd(__hadd(__hmul(ha12, m1m1), __hmul(ha21, m1m1)), __hmul(h_2a11, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=74;
            temp = __hadd(__hadd(__hmul(ha12, m2m2), __hmul(ha21, m2m2)), __hmul(h_2a22, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=75;
            temp = __hadd(__hadd(__hmul(ha13, m1m1), __hmul(ha31, m1m1)), __hmul(h_2a11, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=76;
            temp = __hadd(__hadd(__hmul(ha13, m3m3), __hmul(ha31, m3m3)), __hmul(h_2a33, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=77;
            temp = __hadd(__hadd(__hmul(ha23, m2m2), __hmul(ha32, m2m2)), __hmul(h_2a22, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=78;
            temp = __hadd(__hadd(__hmul(ha23, m3m3), __hmul(ha32, m3m3)), __hmul(h_2a33, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=91;
            temp = __hadd(__hmul(ha12, m1m2), __hmul(ha21, m1m2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=93;
            temp = __hadd(__hmul(ha13, m1m3), __hmul(ha31, m1m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=99;
            temp = __hadd(__hmul(ha23, m2m3), __hmul(ha32, m2m3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=109;
            temp = __hadd(__hadd(__hadd(__hmul(ha12, m3m3), __hmul(ha21, m3m3)), __hadd(__hmul(h_2a13, m2m3), __hmul(h_2a23, m1m3))), __hadd(__hadd(__hmul(h_2a31, m2m3), __hmul(h_2a32, m1m3)), __hmul(h_2a33, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=110;
            temp = __hadd(__hadd(__hadd(__hmul(ha13, m2m2), __hmul(ha31, m2m2)), __hadd(__hmul(h_2a12, m2m3), __hmul(h_2a21, m2m3))), __hadd(__hadd(__hmul(h_2a22, m1m3), __hmul(h_2a23, m1m2)), __hmul(h_2a32, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=114;
            temp = __hadd(__hadd(__hadd(__hmul(ha23, m1m1), __hmul(ha32, m1m1)), __hadd(__hmul(h_2a11, m2m3), __hmul(h_2a12, m1m3))), __hadd(__hadd(__hmul(h_2a13, m1m2), __hmul(h_2a21, m1m3)), __hmul(h_2a31, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=154;
            temp = __hadd(__hadd(__hmul(hb12, m1m2), __hmul(hb21, m1m2)), __hadd(__hmul(ha12, n1n2), __hmul(ha21, n1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=156;
            temp = __hadd(__hadd(__hmul(hb12, m1m3), __hmul(hb21, m1m3)), __hadd(__hmul(ha13, n1n2), __hmul(ha31, n1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=158;
            temp = __hadd(__hadd(__hmul(hb12, m2m3), __hmul(hb21, m2m3)), __hadd(__hmul(ha23, n1n2), __hmul(ha32, n1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=160;
            temp = __hadd(__hadd(__hmul(hb13, m1m2), __hmul(hb31, m1m2)), __hadd(__hmul(ha12, n1n3), __hmul(ha21, n1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=162;
            temp = __hadd(__hadd(__hmul(hb13, m1m3), __hmul(hb31, m1m3)), __hadd(__hmul(ha13, n1n3), __hmul(ha31, n1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=164;
            temp = __hadd(__hadd(__hmul(hb13, m2m3), __hmul(hb31, m2m3)), __hadd(__hmul(ha23, n1n3), __hmul(ha32, n1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=166;
            temp = __hadd(__hadd(__hmul(hb23, m1m2), __hmul(hb32, m1m2)), __hadd(__hmul(ha12, n2n3), __hmul(ha21, n2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=168;
            temp = __hadd(__hadd(__hmul(hb23, m1m3), __hmul(hb32, m1m3)), __hadd(__hmul(ha13, n2n3), __hmul(ha31, n2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=170;
            temp = __hadd(__hadd(__hmul(hb23, m2m3), __hmul(hb32, m2m3)), __hadd(__hmul(ha23, n2n3), __hmul(ha32, n2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=172;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n1), __hmul(h_2a12, m1n1)), __hmul(h_2a21, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=173;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a12, m2n1), __hmul(h_2a21, m2n1)), __hmul(h_2a22, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=174;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n1), __hmul(h_2a13, m1n1)), __hmul(h_2a31, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=175;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a13, m3n1), __hmul(h_2a31, m3n1)), __hmul(h_2a33, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=178;
            temp = __hneg(__hadd(__hmul(ha12, m1n1), __hmul(ha21, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=179;
            temp = __hneg(__hadd(__hmul(ha12, m1n2), __hmul(ha21, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=180;
            temp = __hneg(__hadd(__hmul(ha12, m2n1), __hmul(ha21, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=181;
            temp = __hneg(__hadd(__hmul(ha12, m2n2), __hmul(ha21, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=182;
            temp = __hneg(__hadd(__hmul(ha13, m1n1), __hmul(ha31, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=183;
            temp = __hneg(__hadd(__hmul(ha13, m1n2), __hmul(ha31, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=184;
            temp = __hneg(__hadd(__hmul(ha13, m3n1), __hmul(ha31, m3n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=185;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n1), __hmul(h_2a23, m2n1)), __hmul(h_2a32, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=186;
            temp = __hneg(__hadd(__hmul(ha13, m3n2), __hmul(ha31, m3n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=187;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a23, m3n1), __hmul(h_2a32, m3n1)), __hmul(h_2a33, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=191;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n2), __hmul(h_2a12, m1n2)), __hmul(h_2a21, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=193;
            temp = __hneg(__hadd(__hmul(ha12, m1n3), __hmul(ha21, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=194;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a12, m2n2), __hmul(h_2a21, m2n2)), __hmul(h_2a22, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=197;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n2), __hmul(h_2a13, m1n2)), __hmul(h_2a31, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=198;
            temp = __hneg(__hadd(__hmul(ha12, m2n3), __hmul(ha21, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=200;
            temp = __hneg(__hadd(__hmul(ha13, m1n3), __hmul(ha31, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=202;
            temp = __hneg(__hadd(__hmul(ha23, m2n1), __hmul(ha32, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=203;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a13, m3n2), __hmul(h_2a31, m3n2)), __hmul(h_2a33, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=204;
            temp = __hneg(__hadd(__hmul(ha23, m2n2), __hmul(ha32, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=205;
            temp = __hneg(__hadd(__hmul(ha23, m3n1), __hmul(ha32, m3n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=206;
            temp = __hneg(__hadd(__hmul(ha13, m3n3), __hmul(ha31, m3n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=207;
            temp = __hneg(__hadd(__hmul(ha23, m3n2), __hmul(ha32, m3n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=220;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n2), __hmul(h_2a23, m2n2)), __hmul(h_2a32, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=223;
            temp = __hneg(__hadd(__hmul(ha23, m2n3), __hmul(ha32, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=224;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a23, m3n2), __hmul(h_2a32, m3n2)), __hmul(h_2a33, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=226;
            temp = __hneg(__hadd(__hmul(ha23, m3n3), __hmul(ha32, m3n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=228;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n3), __hmul(h_2a12, m1n3)), __hmul(h_2a21, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=229;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a12, m2n3), __hmul(h_2a21, m2n3)), __hmul(h_2a22, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=231;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n3), __hmul(h_2a13, m1n3)), __hmul(h_2a31, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=234;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a13, m3n3), __hmul(h_2a31, m3n3)), __hmul(h_2a33, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=240;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n3), __hmul(h_2a23, m2n3)), __hmul(h_2a32, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=241;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a23, m3n3), __hmul(h_2a32, m3n3)), __hmul(h_2a33, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=244;
            temp = __hadd(__hadd(__hmul(ha12, m1m3), __hmul(ha13, m1m2)), __hadd(__hmul(ha21, m1m3), __hmul(ha31, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=245;
            temp = __hadd(__hadd(__hmul(ha12, m2m3), __hmul(ha21, m2m3)), __hadd(__hmul(ha23, m1m2), __hmul(ha32, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=246;
            temp = __hadd(__hadd(__hmul(ha13, m2m3), __hmul(ha23, m1m3)), __hadd(__hmul(ha31, m2m3), __hmul(ha32, m1m3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=262;
            temp = __hadd(__hadd(__hmul(h_2a11, m2m2), __hmul(h_2a22, m1m1)), __hadd(__hmul(h_2a12, m1m2), __hmul(h_2a21, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=263;
            temp = __hadd(__hadd(__hmul(h_2a11, m3m3), __hmul(h_2a33, m1m1)), __hadd(__hmul(h_2a13, m1m3), __hmul(h_2a31, m1m3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=264;           
            temp = __hadd(__hadd(__hadd(__hmul(ha13, m2m2), __hmul(ha31, m2m2)), __hadd(__hmul(ha12, m2m3), __hmul(ha21, m2m3))), __hadd(__hadd(__hmul(h_2a22, m1m3), __hmul(ha23, m1m2)), __hmul(ha32, m1m2)));
            temp = __hmul(m1m3, h_2a22);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=266;
            temp = __hadd(__hadd(__hadd(__hmul(ha12, m3m3), __hmul(ha21, m3m3)), __hadd(__hmul(ha13, m2m3), __hmul(ha23, m1m3))), __hadd(__hadd(__hmul(ha31, m2m3), __hmul(ha32, m1m3)), __hmul(h_2a33, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=268;
            temp = __hadd(__hadd(__hadd(__hmul(ha23, m1m1), __hmul(ha32, m1m1)), __hadd(__hmul(h_2a11, m2m3), __hmul(ha12, m1m3))), __hadd(__hadd(__hmul(ha13, m1m2), __hmul(ha21, m1m3)), __hmul(ha31, m1m2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=270;
            temp = __hadd(__hadd(__hmul(h_2a22, m3m3), __hmul(h_2a33, m2m2)), __hadd(__hmul(h_2a23, m2m3), __hmul(h_2a32, m2m3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=289;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2a12, m3n1), __hmul(h_2a13, m2n1)), __hmul(h_2a21, m3n1)), __hadd(__hadd(__hmul(h_2a23, m1n1), __hmul(h_2a31, m2n1)), __hmul(h_2a32, m1n1))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=290;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n2), __hmul(ha12, m1n2)), __hmul(ha21, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=291;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n2), __hmul(ha13, m1n2)), __hmul(ha31, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=292;
            temp = __hneg(__hadd(__hadd(__hmul(ha12, m2n2), __hmul(ha21, m2n2)), __hmul(h_2a22, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=293;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n2), __hmul(ha21, m3n2)), __hmul(ha23, m1n2)), __hmul(ha32, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=294;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n2), __hmul(ha13, m2n2)), __hmul(ha21, m3n2)), __hmul(ha31, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=295;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha13, m2n2), __hmul(ha23, m1n2)), __hmul(ha31, m2n2)), __hmul(ha32, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=296;
            temp = __hneg(__hadd(__hadd(__hmul(ha13, m3n2), __hmul(ha31, m3n2)), __hmul(h_2a33, m1n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=297;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n1), __hmul(ha12, m1n1)), __hmul(ha21, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=298;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m2n3), __hmul(ha12, m1n3)), __hmul(ha21, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=299;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n1), __hmul(ha13, m1n1)), __hmul(ha31, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=300;
            temp = __hneg(__hadd(__hadd(__hmul(ha12, m2n1), __hmul(ha21, m2n1)), __hmul(h_2a22, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=301;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a11, m3n3), __hmul(ha13, m1n3)), __hmul(ha31, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=302;
            temp = __hneg(__hadd(__hadd(__hmul(ha12, m2n3), __hmul(ha21, m2n3)), __hmul(h_2a22, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=303;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n1), __hmul(ha13, m2n1)), __hmul(ha21, m3n1)), __hmul(ha31, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=304;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n1), __hmul(ha21, m3n1)), __hmul(ha23, m1n1)), __hmul(ha32, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=305;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha13, m2n1), __hmul(ha23, m1n1)), __hmul(ha31, m2n1)), __hmul(ha32, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=306;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n3), __hmul(ha21, m3n3)), __hmul(ha23, m1n3)), __hmul(ha32, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=307;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha12, m3n3), __hmul(ha13, m2n3)), __hmul(ha21, m3n3)), __hmul(ha31, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=308;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(ha13, m2n3), __hmul(ha23, m1n3)), __hmul(ha31, m2n3)), __hmul(ha32, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=309;
            temp = __hneg(__hadd(__hadd(__hmul(ha13, m3n1), __hmul(ha31, m3n1)), __hmul(h_2a33, m1n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=310;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n2), __hmul(ha23, m2n2)), __hmul(ha32, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=311;
            temp = __hneg(__hadd(__hadd(__hmul(ha13, m3n3), __hmul(ha31, m3n3)), __hmul(h_2a33, m1n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=312;
            temp = __hneg(__hadd(__hadd(__hmul(ha23, m3n2), __hmul(ha32, m3n2)), __hmul(h_2a33, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=313;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2a12, m3n2), __hmul(h_2a13, m2n2)), __hmul(h_2a21, m3n2)), __hadd(__hadd(__hmul(h_2a23, m1n2), __hmul(h_2a31, m2n2)), __hmul(h_2a32, m1n2))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=314;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n1), __hmul(ha23, m2n1)), __hmul(ha32, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=315;
            temp = __hneg(__hadd(__hadd(__hmul(h_2a22, m3n3), __hmul(ha23, m2n3)), __hmul(ha32, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=316;
            temp = __hneg(__hadd(__hadd(__hmul(ha23, m3n1), __hmul(ha32, m3n1)), __hmul(h_2a33, m2n1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=317;
            temp = __hneg(__hadd(__hadd(__hmul(ha23, m3n3), __hmul(ha32, m3n3)), __hmul(h_2a33, m2n3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=343;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2a12, m3n3), __hmul(h_2a13, m2n3)), __hmul(h_2a21, m3n3)), __hadd(__hadd(__hmul(h_2a23, m1n3), __hmul(h_2a31, m2n3)), __hmul(h_2a32, m1n3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=345;
            temp = __hneg(__hadd(__hadd(__hmul(ha23, m3n2), __hmul(ha32, m3n2)), __hmul(h_2a33, m2n2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=346;
            temp = hb11;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=347;
            temp = hb22;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=348;
            temp = hb33;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=349;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(h_2b11, hn1), __hmul(hb12, hn2)), __hmul(hb13, hn3)), __hadd(__hmul(hb21, hn2), __hmul(hb31, hn3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=350;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb12, hn1), __hmul(hb21, hn1)), __hmul(h_2b22, hn2)), __hadd(__hmul(hb23, hn3), __hmul(hb32, hn3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=351;
            temp = __hneg(__hadd(__hadd(__hadd(__hmul(hb13, hn1), __hmul(hb23, hn2)), __hmul(hb31, hn1)), __hadd(__hmul(hb32, hn2), __hmul(h_2b33, hn3))));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=352;
            temp = __hadd(hb12, hb21);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=353;
            temp = __hadd(hb13, hb31);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=354;
            temp = __hadd(hb23, hb32);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=355;
            temp = ha11;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=357;
            temp = ha22;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=360;
            temp = ha33;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=364;
            temp = __hmul(h_2b11, hm1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=365;
            temp = __hmul(h_2b11, hm2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=366;
            temp = __hadd(__hmul(hb12, hm1), __hmul(hb21, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=367;
            temp = __hmul(h_2b11, hm3);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=368;
            temp = __hadd(__hmul(hb12, hm2), __hmul(hb21, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=369;
            temp = __hadd(__hmul(hb13, hm1), __hmul(hb31, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=370;
            temp = __hadd(__hmul(hb12, hm3), __hmul(hb21, hm3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=371;
            temp = __hadd(__hmul(hb13, hm2), __hmul(hb31, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=372;
            temp = __hadd(__hmul(hb13, hm3), __hmul(hb31, hm3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=377;
            temp = __hmul(h_2b22, hm1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=379;
            temp = __hmul(h_2b22, hm2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=380;
            temp = __hadd(__hmul(hb23, hm1), __hmul(hb32, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=382;
            temp = __hmul(h_2b22, hm3);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=383;
            temp = __hadd(__hmul(hb23, hm2), __hmul(hb32, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=384;
            temp = __hadd(__hmul(hb23, hm3), __hmul(hb32, hm3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=388;
            temp = __hmul(h_2b33, hm1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=389;
            temp = __hmul(h_2b33, hm2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=390;
            temp = __hmul(h_2b33, hm3);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=391;
            temp = __hmul(h_2a11, hm1);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=393;
            temp = __hmul(h_2a22, hm2);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=396;
            temp = __hmul(h_2a33, hm3);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=400;
            temp = __hneg(__hmul(h_2a11, hn1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=401;
            temp = __hneg(__hmul(h_2a11, hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=402;
            temp = __hneg(__hmul(h_2a22, hn1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=403;
            temp = __hneg(__hmul(h_2a11, hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=404;
            temp = __hneg(__hmul(h_2a22, hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=405;
            temp = __hneg(__hmul(h_2a33, hn1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=406;
            temp = __hneg(__hmul(h_2a22, hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=407;
            temp = __hneg(__hmul(h_2a33, hn2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=408;
            temp = __hneg(__hmul(h_2a33, hn3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=409;
            temp = __hadd(ha12, ha21);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=410;
            temp = __hadd(ha13, ha31);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=411;
            temp = __hadd(ha23, ha32);
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=418;
            temp = __hneg(__hadd(__hmul(h_2a12, hn1), __hmul(h_2a21, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=419;
            temp = __hneg(__hadd(__hmul(h_2a13, hn1), __hmul(h_2a31, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=422;
            temp = __hneg(__hadd(__hmul(ha12, hn1), __hmul(ha21, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=424;
            temp = __hneg(__hadd(__hmul(ha12, hn2), __hmul(ha21, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=426;
            temp = __hneg(__hadd(__hmul(ha13, hn1), __hmul(ha31, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=428;
            temp = __hneg(__hadd(__hmul(ha13, hn2), __hmul(ha31, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=430;
            temp = __hneg(__hadd(__hmul(h_2a23, hn1), __hmul(h_2a32, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=435;
            temp = __hneg(__hadd(__hmul(h_2a12, hn2), __hmul(h_2a21, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=436;
            temp = __hneg(__hadd(__hmul(ha12, hn3), __hmul(ha21, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=442;
            temp = __hneg(__hadd(__hmul(h_2a13, hn2), __hmul(h_2a31, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=443;
            temp = __hneg(__hadd(__hmul(ha13, hn3), __hmul(ha31, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=445;
            temp = __hneg(__hadd(__hmul(ha23, hn1), __hmul(ha32, hn1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=447;
            temp = __hneg(__hadd(__hmul(ha23, hn2), __hmul(ha32, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=463;
            temp = __hneg(__hadd(__hmul(h_2a23, hn2), __hmul(h_2a32, hn2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=464;
            temp = __hneg(__hadd(__hmul(ha23, hn3), __hmul(ha32, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=468;
            temp = __hneg(__hadd(__hmul(h_2a12, hn3), __hmul(h_2a21, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=471;
            temp = __hneg(__hadd(__hmul(h_2a13, hn3), __hmul(h_2a31, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=478;
            temp = __hneg(__hadd(__hmul(h_2a23, hn3), __hmul(h_2a32, hn3)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=481;
            temp = h_2a11;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=489;
            temp = h_2a22;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=502;
            temp = h_2a33;
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=508;
            temp = __hadd(__hadd(__hmul(h_2a11, hm2), __hmul(h_2a12, hm1)), __hmul(h_2a21, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=509;
            temp = __hadd(__hadd(__hmul(h_2a12, hm2), __hmul(h_2a21, hm2)), __hmul(h_2a22, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=510;
            temp = __hadd(__hadd(__hmul(h_2a11, hm3), __hmul(h_2a13, hm1)), __hmul(h_2a31, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=511;
            temp = __hadd(__hadd(__hmul(h_2a13, hm3), __hmul(h_2a31, hm3)), __hmul(h_2a33, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=514;
            temp = __hadd(__hmul(ha12, hm1), __hmul(ha21, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=516;
            temp = __hadd(__hmul(ha12, hm2), __hmul(ha21, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=518;
            temp = __hadd(__hmul(ha13, hm1), __hmul(ha31, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=520;
            temp = __hadd(__hmul(ha13, hm3), __hmul(ha31, hm3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=521;
            temp = __hadd(__hadd(__hmul(h_2a22, hm3), __hmul(h_2a23, hm2)), __hmul(h_2a32, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=523;
            temp = __hadd(__hadd(__hmul(h_2a23, hm3), __hmul(h_2a32, hm3)), __hmul(h_2a33, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=538;
            temp = __hadd(__hmul(ha23, hm2), __hmul(ha32, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=541;
            temp = __hadd(__hmul(ha23, hm3), __hmul(ha32, hm3));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=580;
            temp = __hadd(__hadd(__hadd(__hmul(h_2a12, hm3), __hmul(h_2a13, hm2)), __hadd(__hmul(h_2a21, hm3), __hmul(h_2a23, hm1))), __hadd(__hmul(h_2a31, hm2), __hmul(h_2a32, hm1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=581;
            temp = __hadd(__hadd(__hmul(h_2a11, hm2), __hmul(ha12, hm1)), __hmul(ha21, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=582;
            temp = __hadd(__hadd(__hmul(h_2a11, hm3), __hmul(ha13, hm1)), __hmul(ha31, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=583;
            temp = __hadd(__hadd(__hmul(ha12, hm2), __hmul(ha21, hm2)), __hmul(h_2a22, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=584;
            temp = __hadd(__hadd(__hmul(ha12, hm3), __hmul(ha21, hm3)), __hadd(__hmul(ha23, hm1), __hmul(ha32, hm1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=585;
            temp = __hadd(__hadd(__hmul(ha12, hm3), __hmul(ha13, hm2)), __hadd(__hmul(ha21, hm3), __hmul(ha31, hm2)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=586;
            temp = __hadd(__hadd(__hmul(ha13, hm2), __hmul(ha23, hm1)), __hadd(__hmul(ha31, hm2), __hmul(ha32, hm1)));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=587;
            temp = __hadd(__hadd(__hmul(ha13, hm3), __hmul(ha31, hm3)), __hmul(h_2a33, hm1));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=601;
            temp = __hadd(__hadd(__hmul(h_2a22, hm3), __hmul(ha23, hm2)), __hmul(ha32, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));

            i=603;
            temp = __hadd(__hadd(__hmul(ha23, hm3), __hmul(ha32, hm3)), __hmul(h_2a33, hm2));
            atomicAdd(&mat[j][i], __hmul(P, temp));
        }
    }
    __syncthreads();

    // Copy the coefficient matrix in each block to the global memory
    if (M >= THREAD_POOL_COEFFICIENTS) {
        if (threadIdx.y < THREAD_POOL_COEFFICIENTS) {
            double index1;
            index1 = blockIdx.y * gridDim.x * (THREAD_POOL_COEFFICIENTS * n_c ) +
                    blockIdx.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                    threadIdx.y * n_c + threadIdx.x * 20;

            index1 = fmod(index1, 4096.0 * THREAD_POOL_COEFFICIENTS * n_c);
            int index = __double2int_rn(index1);
            for (int ii = 0; ii < 20; ii++)
                atomicAdd((B_C + index + ii), __half2float(mat[threadIdx.y][threadIdx.x*20 +ii]));
        }
    } else {
        if (threadIdx.y==0) {
            int index;
            for (int jj=0; jj < THREAD_POOL_COEFFICIENTS; jj++) {
                double index1 = blockIdx.y * gridDim.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                                blockIdx.x * (THREAD_POOL_COEFFICIENTS * n_c) +
                                jj * n_c + threadIdx.x * 20;
                index1 = fmod(index1, 4096.0 * THREAD_POOL_COEFFICIENTS * n_c);
                index = __double2int_rn(index1);

                for (int ii = 0; ii < 20; ii++)
                    atomicAdd((B_C + index + ii),__half2float(mat[jj][threadIdx.x*20 + ii]));
            }
        }
    }

    __syncthreads();
    return;
}

void coeff_reduction(double *d_idata, double *coefficient, int size, int n_c, float norm_factor) {
    int t = size / n_c;
    int stride = n_c;
    int blocksize = 1024;
    dim3 block(blocksize, 1);
    while (t > 1) {
        // there are t numbers to deal with
        dim3 grid(((t + 1)/2 + block.x - 1) / block.x, 1);
        // kernel
        coeff_reduction_kernel<<<grid, block>>>(d_idata, n_c, t, stride);
        cudaDeviceSynchronize();
        //update the state
        t = (t + 1) / 2;
        stride = stride * 2;
    }
    int bytes = n_c * sizeof(double);

    cudaMemcpy(coefficient, d_idata, bytes, cudaMemcpyDeviceToHost);
    //cudaDeviceSynchronize();

    // normalization of the coefficients (value of coeffs does not depend on the number of points)
    for (int i = 0; i < n_c; i++) {
        //printf("%d \t %f - %f\n", i, coefficient[i], coefficient[i]*norm_factor);
        coefficient[i] = coefficient[i] * norm_factor;
    }
}

__global__ void coeff_reduction_kernel(double * B_C, int n_c, int L, int stride) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < (L + 1)/2) {
        if (((L&1) == 0) || (idx != (L + 1)/2 - 1))
        {
            for (int i=0; i <n_c; i++)
                atomicAdd((B_C + idx*2*stride + i), *(B_C + idx*2*stride + stride + i));
        }
    }
    __syncthreads();
}

